<?php

return [
    'months' => [
        'januari' => 'Januari',
        'februari' => 'Februari',
        'maret' => 'Maret',
        'april' => 'April',
        'mei' => 'Mei',
        'juni' => 'Juni',
        'juli' => 'Juli',
        'agustus' => 'Agustus',
        'september' => 'September',
        'oktober' => 'Oktober',
        'november' => 'November',
        'desember' => 'Desember',
    ],
];

<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Livewire\Pages\Auth\LoginPageIndex;
use App\Http\Controllers\Auth\LoginController;
use App\Livewire\Pages\Admin\Dosen\AdminDosenIndex;
use App\Livewire\Pages\Admin\TidakLulusSidangIndex;
use App\Livewire\Pages\Admin\Dosen\AdminDosenManage;
use App\Livewire\Pages\Admin\Nilai\NilaiSidangIndex;
use App\Livewire\Pages\Admin\ProgressMahasiswaIndex;
use App\Livewire\Pages\Admin\Jadwal\JadwalSidangIndex;
use App\Livewire\Pages\Admin\Nilai\NilaiProposalIndex;
use App\Livewire\Pages\Admin\Kaprodi\AdminKaprodiIndex;
use App\Livewire\Pages\Admin\Nilai\NilaiPrasidangIndex;
use App\Livewire\Pages\Admin\Periode\AdminPeriodeIndex;
use App\Livewire\Pages\Admin\Ruangan\AdminRuanganIndex;
use App\Livewire\Pages\Admin\Periode\AdminPeriodeCreate;
use App\Livewire\Pages\Admin\Periode\AdminPeriodeManage;
use App\Livewire\Pages\Dosen\DosenWali\DosenDoswalIndex;
use App\Livewire\Pages\Admin\Hakakses\AdminHakAksesIndex;
use App\Livewire\Pages\Admin\Jadwal\JadwalPrasidangIndex;
use App\Livewire\Pages\Mahasiswa\TaPa\MahasiswaTaPaIndex;
use App\Livewire\Pages\Admin\Dashboard\AdminDashboardIndex;
use App\Livewire\Pages\Admin\Mahasiswa\AdminMahasiswaIndex;
use App\Livewire\Pages\Admin\Nilaimutu\AdminNilaimutuIndex;
use App\Livewire\Pages\Admin\Roledosen\AdminRoledosenIndex;
use App\Livewire\Pages\Dosen\Dashboard\DosenDashboardIndex;
use App\Livewire\Pages\Dosen\DosenPenguji\DosenDospujIndex;
use App\Livewire\Pages\Mahasiswa\Nilai\MahasiswaNilaiIndex;
use App\Livewire\Pages\Admin\Mahasiswa\AdminMahasiswaManage;
use App\Livewire\Pages\Mahasiswa\Jadwal\MahasiswaJadwalIndex;
use App\Livewire\Pages\Dosen\DosenPembimbing\DosenDospemIndex;
use App\Livewire\Pages\Kaprodi\Dashboard\KaprodiDashboardIndex;
use App\Livewire\Pages\Admin\Rolepengguna\AdminRolepenggunaIndex;
use App\Livewire\Pages\Admin\KoordinatorPA\UploadMahasiswa\Sidang;
use App\Livewire\Pages\Mahasiswa\Dashboard\MahasiswaDashboardIndex;
use App\Livewire\Pages\Admin\KoordinatorPA\KelolaKomponenNilaiIndex;
use App\Livewire\Pages\Admin\KoordinatorPA\UploadMahasiswa\Proposal;
use App\Livewire\Pages\Admin\KoordinatorPA\UploadMahasiswa\Prasidang;
use App\Livewire\Pages\Superadmin\Dashboard\SuperadminDashboardIndex;
use App\Livewire\Pages\Admin\KoordinatorPA\UploadMahasiswa\PrasidangCreate;
use App\Livewire\Pages\Admin\KoordinatorPA\UploadMahasiswa\PrasidangJadwal;
use App\Livewire\Pages\Admin\KoordinatorPA\UploadMahasiswa\SidangManageIndex;
use App\Livewire\Pages\Admin\KoordinatorPA\UploadMahasiswa\ProposalManageIndex;
use App\Livewire\Pages\Admin\KoordinatorPA\UploadMahasiswa\PrasidangManageIndex;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
// Route::get('/', function () {
//     return redirect('/login');
// });
Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::middleware(['middleware' => 'admin'])->group(function () {
    Route::get('/admin', AdminDashboardIndex::class)->name('admin.dashboard');

    Route::prefix('admin/')->group(function () {
        Route::prefix('/periode')->group(function () {
            Route::get('/', AdminPeriodeIndex::class)->name('admin.periode.index');
            Route::get('/manage', AdminPeriodeCreate::class)->name('periode.create');
            Route::get('/manage/{id_periode}', AdminPeriodeManage::class)->name('periode.manage');
        });
        Route::get('/Role-Dosen', AdminRoledosenIndex::class)->name('admin.roledosen.index');
        Route::get('/Nilai-mutu', AdminNilaimutuIndex::class)->name('admin.nilaimutu.index');
        Route::get('/Kaprodi', AdminKaprodiIndex::class)->name('admin.kaprodi.index');
        Route::prefix('/Dosen')->group(function () {
            Route::get('/', AdminDosenIndex::class)->name('admin.dosen.index');
            Route::get('/manage', AdminDosenManage::class)->name('admin.dosen.manage');
        });

        Route::prefix('/mahasiswa')->group(function () {
            Route::get('/create', AdminMahasiswaManage::class)->name('admin.mahasiswa.create');
            Route::get('/', AdminMahasiswaIndex::class)->name('admin.mahasiswa.index');
        });

        Route::prefix('/Koordinator-PA')->group(function () {
            Route::get('/', KelolaKomponenNilaiIndex::class)->name('KoordinatorPA.KKN.index');
            Route::prefix('/Upload-Mahasiswa')->group(function () {
                Route::get('/sidang', Sidang::class)->name('upload.mahasiswa.sidang.index');
                Route::get('/sidang-manage', SidangManageIndex::class)->name('upload.mahasiswa.sidang.manage');
                Route::get('/prasidang', Prasidang::class)->name('upload.mahasiswa.prasidang.index');
                Route::get('/prasidang-manage/{id_prasidang}', PrasidangCreate::class)->name('upload.mahasiswa.prasidang.manage');
                Route::get('/jadwal-prasidang/{id}', PrasidangJadwal::class)->name('upload.mahasiswa.prasidang.jadwal');
                Route::get('/prasidang-manage', PrasidangManageIndex::class)->name('upload.mahasiswa.prasidang.create');
                Route::get('/proposal', Proposal::class)->name('upload.mahasiswa.proposal.index');
                Route::get('/proposal-manage', ProposalManageIndex::class)->name('upload.mahasiswa.proposal.manage');
            });
            Route::prefix('/Jadwal')->group(function () {
                Route::get('/sidang', JadwalSidangIndex::class)->name('jadwal.sidang.index');
                Route::get('/prasidang', JadwalPrasidangIndex::class)->name('jadwal.prasidang.index');
            });
            Route::prefix('/Nilai')->group(function () {
                Route::get('/sidang', NilaiSidangIndex::class)->name('nilai.sidang.index');
                Route::get('/prasidang', NilaiPrasidangIndex::class)->name('nilai.prasidang.index');
                Route::get('/proposal', NilaiProposalIndex::class)->name('nilai.proposal.index');
            });
            Route::get('/Tidak-Lulus-Sidang', TidakLulusSidangIndex::class)->name('tidak.lulus.sidang.index');
            Route::get('/Progress-Mahasiswa', ProgressMahasiswaIndex::class)->name('progress.mahasiswa.index');

        });

        Route::get('/Ruangan', AdminRuanganIndex::class)->name('admin.ruangan.index');
        Route::get('/Hak-Akses', AdminHakAksesIndex::class)->name('admin.Hakakses.index');
        Route::get('/Role-Pengguna', AdminRolepenggunaIndex::class)->name('admin.rolepengguna.index');

    });

});
    Route::get('/dosen', DosenDashboardIndex::class)->name('dosen.dashboard');
    Route::prefix('/DosenPembimbing')->group(function () {
        Route::get('/', DosenDospemIndex::class)->name('dosen.dospem.index');
    });
    Route::prefix('/DosenPenguji')->group(function () {
        Route::get('/', DosenDospujIndex::class)->name('dosen.dospuj.index');
    });
    Route::prefix('/DosenWali')->group(function () {
        Route::get('/', DosenDoswalIndex::class)->name('dosen.doswal.index');
    });


Route::middleware(['middleware' => 'kaprodi'])->group(function () {
    Route::get('/kaprodi', KaprodiDashboardIndex::class)->name('kaprodi.dashboard');
});

Route::middleware(['middleware' => 'mahasiswa'])->group(function () {
    Route::get('/mahasiswa', MahasiswaDashboardIndex::class)->name('mahasiswa.dashboard');
    Route::prefix('/NilaiMahasiswa')->group(function () {
        Route::get('/', MahasiswaNilaiIndex::class)->name('mahasiswa.nilai.index');
    });
    Route::prefix('/JadwalMahasiswa')->group(function () {
        Route::get('/', MahasiswaJadwalIndex::class)->name('mahasiswa.jadwal.index');
    });
    Route::prefix('/TA-PA')->group(function () {
        Route::get('/', MahasiswaTaPaIndex::class)->name('mahasiswa.tapa.index');
    });
});

Route::middleware(['middleware' => 'superadmin'])->group(function () {
    Route::get('/SuperAdmin', SuperadminDashboardIndex::class)->name('superadmin.dashboard');
});

<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "mahasiswa.dashboard",
            "Mahasiswa" => 'mahasiswa.nilai.index'
        ],
        'page_title' => 'Nilai Mahasiswa',
        ])
    @endcomponent
    @endsection
    @livewire('components.mahasiswa.nilai.mahasiswa-nilai-datatable')
</div>

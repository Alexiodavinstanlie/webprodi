<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "mahasiswa.dashboard",
            "Mahasiswa" => 'mahasiswa.tapa.index'
        ],
        'page_title' => 'TA dan PA Mahasiswa',
        ])
    @endcomponent
    @endsection
    @livewire('components.mahasiswa.ta-pa.mahasiswa-ta-pa-datatable')
</div>

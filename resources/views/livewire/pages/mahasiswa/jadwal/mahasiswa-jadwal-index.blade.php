<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "mahasiswa.dashboard",
            "Mahasiswa" => 'mahasiswa.jadwal.index'
        ],
        'page_title' => 'Jadwal Mahasiswa',
        ])
    @endcomponent
    @endsection
    @livewire('components.mahasiswa.jadwal.mahasiswa-jadwal-datatable')
</div>

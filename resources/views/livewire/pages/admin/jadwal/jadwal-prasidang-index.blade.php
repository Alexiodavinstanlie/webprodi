<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "admin.dashboard",
            "Koordinator PA" => 'jadwal.prasidang.index'
        ],
        'page_title' => 'Jadwal Prasidang',
        ])
    @endcomponent
    @endsection
    @livewire('components.admin.jadwal.jadwal-prasidang-datatable')
</div>

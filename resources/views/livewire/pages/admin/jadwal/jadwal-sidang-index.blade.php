<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "admin.dashboard",
            "Koordinator PA" => 'jadwal.sidang.index'
        ],
        'page_title' => 'Jadwal Sidang',
        ])
    @endcomponent
    @endsection
    @livewire('components.admin.jadwal.jadwal-sidang-datatable')
</div>

<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Admin' => "admin.dashboard",
            "Role Dosen" => 'admin.roledosen.index'
        ],
        'page_title' => 'Role Dosen',
        ])
    @endcomponent
    @endsection
    @livewire('components.admin.roledosen.roledosen-datatable')
</div>

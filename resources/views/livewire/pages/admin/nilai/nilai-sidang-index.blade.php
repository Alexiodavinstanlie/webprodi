<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "admin.dashboard",
            "Koordinator PA" => 'nilai.sidang.index'
        ],
        'page_title' => 'Nilai Sidang',
        ])
    @endcomponent
    @endsection
    @livewire('components.admin.nilai.nilai-sidang-datatable')
</div>

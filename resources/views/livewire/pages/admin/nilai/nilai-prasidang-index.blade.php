<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "admin.dashboard",
            "Koordinator PA" => 'nilai.prasidang.index'
        ],
        'page_title' => 'Nilai Prasidang',
        ])
    @endcomponent
    @endsection
    @livewire('components.admin.nilai.nilai-prasidang-datatable')
</div>

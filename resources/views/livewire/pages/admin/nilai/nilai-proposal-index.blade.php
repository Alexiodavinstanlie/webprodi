<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "admin.dashboard",
            "Koordinator PA" => 'nilai.proposal.index'
        ],
        'page_title' => 'Nilai Proposal',
        ])
    @endcomponent
    @endsection
    @livewire('components.admin.nilai.nilai-proposal-datatable')
</div>

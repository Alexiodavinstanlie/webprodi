<div>
    @section('breadcrumbs')
        @component('components.breadcrumb', [
            'link_items' => [
                'Dashboard' => 'admin.dashboard',
                'Admin' => 'index',
            ],
            'page_title' => 'Dashboard',
        ])
        @endcomponent
    @endsection
    @livewire('components.admin.dashboard.admin-dashboard-datatable')
    @push('scripts')
    <script>
        window.addEventListener("closeModalTahunAjaran", event => {
            $("#tahunajaranModal").modal("hide");
        })
        window.addEventListener("openModalTahunAjaran", event => {
            $("#tahunajaranModal").modal("open");
        })
    </script>
    @endpush
</div>

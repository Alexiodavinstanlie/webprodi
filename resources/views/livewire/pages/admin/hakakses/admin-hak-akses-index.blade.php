<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "admin.dashboard",
            "Admin" => 'admin.Hakakses.index'
        ],
        'page_title' => 'Hak Akses',
        ])
    @endcomponent
    @endsection
    @livewire('components.admin.hakakses.hak-akses-datatable')
</div>

<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "admin.dashboard",
            "Admin" => 'admin.ruangan.index'
        ],
        'page_title' => 'Ruangan',
        ])
    @endcomponent
    @endsection
    @livewire('components.admin.ruangan.ruangan-datatable')
</div>

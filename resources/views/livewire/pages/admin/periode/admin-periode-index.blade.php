<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "admin.dashboard",
            "Admin" => 'admin.periode.index'
        ],
        'page_title' => 'Periode',
        ])
    @endcomponent
    @endsection
    @livewire('components.admin.periode.periode-datatable')
</div>

<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "admin.dashboard",
            "Koordinator PA" => 'tidak.lulus.sidang.index'
        ],
        'page_title' => 'Tidak Lulus Sidang',
        ])
    @endcomponent
    @endsection
    @livewire('components.admin.tidak-lulus-sidang-datatable')
</div>

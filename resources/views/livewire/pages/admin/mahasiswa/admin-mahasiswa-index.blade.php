<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "admin.dashboard",
            "Admin" => 'admin.mahasiswa.index'
        ],
        'page_title' => 'Mahasiswa',
        ])
    @endcomponent
    @endsection
    @livewire('components.admin.mahasiswa.mahasiswa-datatable')
</div>

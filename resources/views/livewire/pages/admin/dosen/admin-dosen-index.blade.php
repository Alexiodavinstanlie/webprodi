<div>
    <div>
        @section('breadcrumbs')
        @component('components.breadcrumb',[
            'link_items' =>  [
                'Dashboard' => "admin.dashboard",
                "Admin" => 'admin.dosen.index'
            ],
            'page_title' => 'Dosen',
            ])
        @endcomponent
        @endsection
    @livewire('components.admin.dosen.dosen-datatable')
</div>

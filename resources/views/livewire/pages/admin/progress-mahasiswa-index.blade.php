<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "admin.dashboard",
            "Koordinator PA" => 'progress.mahasiswa.index'
        ],
        'page_title' => 'Progress Mahasiswa',
        ])
    @endcomponent
    @endsection
    @livewire('components.admin.progress-mahasiswa-datatable')
</div>

<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "admin.dashboard",
            "Koordinator PA" => 'KoordinatorPA.KKN.index'
        ],
        'page_title' => 'Kelola Komponen Nilai',
        ])
    @endcomponent
    @endsection
    @livewire('components.admin.koordinator-p-a.kelola-komponen-nilai-datatable')
    <script>
        window.addEventListener("closeModalNilai", event => {
            $("#tambahNilaiModal").modal("hide");
        })
        window.addEventListener("openModalNilai", event => {
            $("#tambahNilaiModal").modal("open");
        })
        window.addEventListener("closeModalNilaiPrasidang", event => {
            $("#tambahNilaiPrasidangModal").modal("hide");
        })
        window.addEventListener("openModalNilaiPrasidang", event => {
            $("#tambahNilaiPrasidangModal").modal("open");
        })
        window.addEventListener("closeModalNilaiSidang", event => {
            $("#tambahNilaiSidangModal").modal("hide");
        })
        window.addEventListener("openModalNilaiSidang", event => {
            $("#tambahNilaiSidangModal").modal("open");
        })
    </script>
</div>

<<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "admin.dashboard",
            "Koordinator PA" => 'upload.mahasiswa.proposal.index'
        ],
        'page_title' => 'Upload Mahasiswa Proposal',
        ])
    @endcomponent
    @endsection
    @livewire('components.admin.koordinator-p-a.upload-mahasiswa.upload-proposal-manage')
</div>

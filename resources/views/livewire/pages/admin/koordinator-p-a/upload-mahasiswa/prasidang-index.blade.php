<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "admin.dashboard",
            "Koordinator PA" => 'upload.mahasiswa.prasidang.index'
        ],
        'page_title' => 'Upload Mahasiswa Prasidang',
        ])
    @endcomponent
    @endsection
    @livewire('components.admin.koordinator-p-a.upload-mahasiswa.upload-mahasiswa-prasidang-datatable')
</div>

<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "admin.dashboard",
            "Koordinator PA" => 'upload.mahasiswa.sidang.index'
        ],
        'page_title' => 'Upload Mahasiswa Sidang',
        ])
    @endcomponent
    @endsection
    @livewire('components.admin.koordinator-p-a.upload-mahasiswa.upload-sidang-manage')
</div>

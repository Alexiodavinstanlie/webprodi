<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "admin.dashboard",
            "Admin" => 'admin.nilaimutu.index'
        ],
        'page_title' => 'Nilai Mutu',
        ])
    @endcomponent
    @endsection
    @livewire('components.admin.nilaimutu.nilaimutu-datatable')
    @push('scripts')
    <script>
        window.addEventListener("closeModalNilaimutu", event => {
            $("#nilaimutuModal").modal("hide");
        })
        window.addEventListener("openModalNilaiMutu", event => {
            $("#nilaimutuModal").modal("open");
        })
    </script>
    @endpush
</div>

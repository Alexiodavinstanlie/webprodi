<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "admin.dashboard",
            "Admin" => 'admin.rolepengguna.index'
        ],
        'page_title' => 'Role Pengguna',
        ])
    @endcomponent
    @endsection
    @livewire('components.admin.rolepengguna.role-pengguna-datatable')
</div>

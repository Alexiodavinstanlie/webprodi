<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "dosen.dashboard",
            "Dosen" => 'dosen.dospuj.index'
        ],
        'page_title' => 'Dosen Penguji',
        ])
    @endcomponent
    @endsection
    @livewire('components.dosen.dosen-penguji.dosen-dospuj-datatable')
</div>

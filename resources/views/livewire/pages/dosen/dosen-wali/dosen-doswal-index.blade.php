<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "dosen.dashboard",
            "Dosen" => 'dosen.doswal.index'
        ],
        'page_title' => 'Dosen Wali',
        ])
    @endcomponent
    @endsection
    @livewire('components.dosen.dosen-wali.dosen-doswal-datatable')
</div>


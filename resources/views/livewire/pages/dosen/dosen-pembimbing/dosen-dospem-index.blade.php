<div>
    @section('breadcrumbs')
        @component('components.breadcrumb', [
            'link_items' => [
                'Dashboard' => 'dosen.dashboard',
                'Dosen' => 'dosen.dospem.index',
            ],
            'page_title' => 'Dosen Pembimbing',
        ])
        @endcomponent
    @endsection
    @livewire('components.dosen.dosen-pembimbing.dosen-dospem-datatable')
</div>

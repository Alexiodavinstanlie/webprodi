<div>
    @section('breadcrumbs')
    @component('components.breadcrumb',[
        'link_items' =>  [
            'Dashboard' => "superadmin.dashboard",
        ],
        'page_title' => 'Super Admin',
        ])
    @endcomponent
    @endsection

    <div>
        <div>
            <div class="card shadow-lg mx-4 mt-5 card-profile-bottom">
                <div class="card-body p-3 mt-2">
                    @component('components.table.table-header', [
                        'title'        => 'Kelola Admin',
                        'with_sort'    => false,
                        'is_function'  => false,
                        'with_filter'  => true,
                        'with_entries' => true,
                        'with_search'  => true,
                        'with_button'  => false,
                        'with_modal' => false,
                    ])
                    @endcomponent
                </div>
            </div>
            <div class="container py-4">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <!-- Card header -->
                            <div class="card-header">
                                <div class="row gx-4">
                                    <div class="col-12">
                                        <div class="card-body p-2">
                                            <h5 class="card-title">Admin</h5>
                                        </div>
                                    </div>
                                    <div class="card mb-6">
                                        <div class="table-responsive">
                                            <table class="table align-items-center mb-0">
                                                <thead>
                                                    <tr>
                                                        <th
                                                            class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            NO</th>
                                                        <th
                                                            class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                            Nama Dosen</th>
                                                        <th
                                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            Kode Dosen</th>
                                                        <th
                                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            NIP</th>
                                                        <th
                                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            Tahun Ajaran</th>
                                                        <th
                                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            Semester</th>
                                                        <th
                                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            Menjabat</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($dosens as $dosen)
                                                    <tr>
                                                        <td>
                                                            <div class="d-flex px-2 py-1">
                                                                <div class="d-flex flex-column justify-content-center">
                                                                    <h6 class="mb-0 text-xs">{{ $num++ }}</h6>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <p class="text-xs font-weight-bold mb-0">
                                                                {{ $dosen->nama_dosen }}
                                                            </p>
                                                        </td>
                                                        <td class="align-middle text-center text-lg">
                                                            <span class="badge badge-sm badge-success">{{ $dosen->singkatan_dosen }}</span>
                                                        </td>
                                                        <td class="align-middle text-center">
                                                            <span class="text-secondary text-xs font-weight-bold">{{ $dosen->nip }}</span>
                                                        </td>
                                                        <td class="align-middle text-center">
                                                            <span
                                                                class="text-secondary text-xs font-weight-bold"> 2012-2021
                                                            </span>
                                                        </td>
                                                        <td class="align-middle text-center">
                                                            <span
                                                                class="text-secondary text-xs font-weight-bold">Ganjil
                                                            </span>
                                                        </td>
                                                        <td class="align-middle text-center">
                                                            <div class="d-flex justify-content-center" wire:loading.attr="disabled">
                                                                <div class="form-check form-switch">
                                                                    <input class="form-check-input"
                                                                           type="checkbox"
                                                                           wire:change="setAdmin({{ $dosen->id }})"
                                                                           {{ ($adminCount == $maxAdminCount && !$dosen->is_admin) ? 'disabled' : '' }}
                                                                           {{ $dosen->is_admin ? 'checked' : '' }}>
                                                                    <label class="form-check-label" for="isAdminToggle"></label>
                                                                </div>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                    @endforeach


                                                </tbody>
                                            </table>
                                            @component('components.table.table-footer', [
                                                'datas' => $dosens,
                                            ])
                                            @endcomponent
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener('livewire:load', function () {
            Livewire.on('showConfirmationModal', () => {
                $('#confirmationModal').modal('show');

                $('#confirmationModal').on('hidden.bs.modal', function () {
                    @this.call('setAdmin');
                });
            });
        });
    </script>

</div>

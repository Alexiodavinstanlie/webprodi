<div>
    <div class="container mt-6">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title mb-5">{{ $prasidang_data ? 'Edit' : 'Input' }} Data Prasi</h5>

                <div class="row">
                    <div class="col-md-4 mb-3">
                        <label for="tahun_ajaran" class="form-label">Tahun Ajaran</label>
                        <input type="text" class="form-control" id="tahun_ajaran" wire:model="tahun_ajaran" disabled>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="semester" class="form-label">Semester</label>
                        <select class="form-select" id="semester" wire:model="semester">
                            <option value="">Pilih Semester</option>
                            <option value="Genap">Genap</option>
                            <option value="Ganjil">Ganjil</option>
                        </select>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="bulan" class="form-label">Bulan</label>
                        <select class="form-select" id="bulan" wire:model="bulan">
                            <option value="">Pilih Bulan</option>
                            @foreach($months as $month)
                                <option value="{{ ucfirst($month) }}">{{ ucfirst($month) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 mb-3">
                        <label for="mahasiswa_id" class="form-label">Nama Mahasiswa</label>
                        <select class="form-select" id="nama_mahasiswa" wire:model="mahasiswa_id">
                            <option value="">Pilih Nama Mahasiswa</option>
                            @foreach ($mahasiswas as $mahasiswa)
                            <option value="{{ $mahasiswa->id }}">{{ $mahasiswa->nama_mahasiswa }}</option>
                        @endforeach
                        </select>
                    </div>
                    {{-- <div class="col-md-4 mb-3">
                        <label for="nim_mahasiswa" class="form-label">NIM Mahasiswa</label>
                        <input type="text" class="form-control" wire:model="nim_mahasiswa" disabled>
                    </div> --}}
                </div>

                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="judul_indo" class="form-label">Judul PA</label>
                        <input type="text" class="form-control" id="judul_indo" wire:model="judul_indo">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="judul_inggris" class="form-label">Title</label>
                        <input type="text" class="form-control" id="judul_inggris" wire:model="judul_inggris">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="pembimbing1_id" class="form-label">Pembimbing 1</label>
                        <select class="form-select" id="pembimbing1_id" wire:model="pembimbing1_id" required
                        onchange="showPembimbing1(this.value)">>
                            <option value="">Pilih Pembimbing 1</option>
                            @foreach($dosens as $dosen)
                            <option value="{{ $dosen->id }}">{{ $dosen->singkatan_dosen }} - {{ $dosen->nama_dosen }}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="pembimbing2_id" class="form-label">Pembimbing 2</label>
                        <select class="form-select" id="pembimbing2_id" wire:model="pembimbing2_id">
                            <option value="">Pilih Pembimbing 2</option>
                            @foreach($dosens as $dosen)
                            <option value="{{ $dosen->id }}">{{ $dosen->singkatan_dosen }} - {{ $dosen->nama_dosen }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="penguji1_id" class="form-label">Penguji 1</label>
                        <select class="form-select" id="penguji1_id" wire:model="penguji1_id">
                            <option value="">Pilih Penguji 1</option>
                            @foreach($dosens as $dosen)
                            <option value="{{ $dosen->id }}">{{ $dosen->singkatan_dosen }} - {{ $dosen->nama_dosen }}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="penguji2_id" class="form-label">Penguji 2</label>
                        <select class="form-select" id="penguji2_id" wire:model="penguji2_id">
                            <option value="">Pilih Penguji 2</option>
                            @foreach($dosens as $dosen)
                            <option value="{{ $dosen->id }}">{{ $dosen->singkatan_dosen }} - {{ $dosen->nama_dosen }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>

                <button wire:click="{{ $prasidang_data ? 'updateData' : 'createData' }}" type="submit" class="btn btn-primary">{{ $prasidang_data ? 'Update' : 'Simpan' }}</button>
            </div>
        </div>
        <div class="card mt-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3 mb-2">
                        <button wire:click="exportTemplate" class="btn btn-primary w-100" type="button" wire:loading.attr="disabled" style="margin-right: 10px;">
                            <div wire:loading wire:target="exportTemplate">
                                <div class="spinner-border text-light spinner-border-sm" role="status"></div>
                            </div>
                            <span class="btn-inner-text"> Download template excel</span>
                        </button>
                    </div>
                    <div class="col-md-3 mb-2">
                        <button class="btn btn-primary w-100" data-bs-toggle="modal" data-bs-target="#modalPeriode">List Periode</button>
                    </div>
                    <div class="col-md-3 mb-2">
                        <button class="btn btn-primary w-100" data-bs-toggle="modal" data-bs-target="#modalMahasiswa">List Mahasiswa</button>
                    </div>
                    <div class="col-md-3 mb-2">
                        <button class="btn btn-primary w-100" data-bs-toggle="modal" data-bs-target="#modalDosen">List Dosen</button>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-6 mb-2">
                        <div class="mb-3">
                            <div class="btn btn-secondary w-100 {{ $file ? 'd-none' : '' }}">
                                <label class="control-label">Unggah File</label>
                                <input wire:model="file" type="file" id="file-{{ $iteration_file }}" class="form-control @error('file') is-invalid @enderror">
                                @error('file') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                            <div class="btn btn-secondary w-100 {{ $file ? '' : 'd-none' }}">
                                <label class="control-label">Unggah File</label>
                                <div class="d-flex justify-content-between align-items-center border rounded p-2">
                                    <div class="col-5">
                                        <i class="fa fa-check text-success"></i><span style="font-size: 12px"> Data Berhasil Diperbarui</span>
                                    </div>
                                    <div class="col-1">
                                        <a wire:click="clearFile" type="button"><i class="fa fa-close text-danger"></i></a>
                                    </div>
                                </div>
                                @error('logo') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <button class="btn btn-success w-100">Simpan</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal List Periode -->
        <div class="modal fade" id="modalPeriode" tabindex="-1" aria-labelledby="modalPeriodeLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalPeriodeLabel">List Periode</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-3">
                            <input type="text" id="searchInput" class="form-control" placeholder="Cari berdasarkan NIM atau Nama">
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Periode</th>
                                        <th scope="col">Semester</th>
                                        <th scope="col">Bulan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($periodes as $periode)
                                    <tr>
                                        <td>{{ $periode->id }}</td>
                                        <td>{{ $periode->periode }}</td>
                                        <td>{{ $periode->semester }}</td>
                                        <td>{{ $periode->bulan }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <p id="noDataMessage" class="text-center font-weight-bolder" style="display: none;">Tidak ada data periode</p>
                        </div>
                        <button type="button" class="btn btn-secondary btn-sm float-end" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <!-- Modal List Mahasiswa -->
        <div class="modal fade" id="modalMahasiswa" tabindex="-1" aria-labelledby="modalMahasiswaLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalMahasiswaLabel">List Mahasiswa</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-3">
                            <input type="text" id="searchInputMahasiswa" class="form-control" placeholder="Cari berdasarkan NIM atau Nama">
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">NIM</th>
                                        <th scope="col">Nama</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($mahasiswas as $mahasiswa)
                                    <tr>
                                        <td>{{ $mahasiswa->id }}</td>
                                        <td>{{ $mahasiswa->nim }}</td>
                                        <td>{{ $mahasiswa->nama_mahasiswa }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <p id="noDataMessageMahasiswa" class="text-center font-weight-bolder" style="display: none;">Tidak ada data mahasiswa</p>
                        </div>
                        <button type="button" class="btn btn-secondary btn-sm float-end" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal List Dosen -->
        <div class="modal fade" id="modalDosen" tabindex="-1" aria-labelledby="modalDosenLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalDosenLabel">List Dosen</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-3">
                            <input type="text" id="searchInputDosen" class="form-control" placeholder="Cari berdasarkan Kode atau Nama Dosen">
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Kode Dosen</th>
                                        <th scope="col">Nama</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($dosens as $dosen)
                                    <tr>
                                        <td>{{ $dosen->id }}</td>
                                        <td>{{ $dosen->singkatan_dosen }}</td>
                                        <td>{{ $dosen->nama_dosen }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <p id="noDataMessageDosen" class="text-center font-weight-bolder" style="display: none;">Tidak ada data dosen</p>
                        </div>
                        <button type="button" class="btn btn-secondary btn-sm float-end" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            const searchInput = document.getElementById('searchInput');
            const tableRows = document.querySelectorAll('tbody tr');
            const noDataMessage = document.getElementById('noDataMessage');

            searchInput.addEventListener('input', function() {
                const searchTerm = searchInput.value.toLowerCase();
                let found = false;

                tableRows.forEach(function(row) {
                    const cells = row.querySelectorAll('td');
                    let rowFound = false;

                    cells.forEach(function(cell) {
                        const cellText = cell.textContent.toLowerCase();
                        if (cellText.includes(searchTerm)) {
                            rowFound = true;
                            found = true;
                        }
                    });

                    if (rowFound) {
                        row.style.display = '';
                    } else {
                        row.style.display = 'none';
                    }
                });

                if (!found) {
                    noDataMessage.style.display = 'block';
                } else {
                    noDataMessage.style.display = 'none';
                }
            });
        });

        document.addEventListener("DOMContentLoaded", function() {
        const searchInputMahasiswa = document.getElementById('searchInputMahasiswa');
        const tableRowsMahasiswa = document.querySelectorAll('#modalMahasiswa table tbody tr');
        const noDataMessageMahasiswa = document.getElementById('noDataMessageMahasiswa');

        searchInputMahasiswa.addEventListener('input', function() {
            const searchTermMahasiswa = searchInputMahasiswa.value.trim().toLowerCase();
            let foundMahasiswa = false;

            tableRowsMahasiswa.forEach(function(row) {
                const cells = row.querySelectorAll('td');
                let rowFound = false;

                cells.forEach(function(cell) {
                    const cellText = cell.textContent.toLowerCase();
                    if (cellText.includes(searchTermMahasiswa)) {
                        rowFound = true;
                        foundMahasiswa = true;
                    }
                });

                if (rowFound) {
                    row.style.display = '';
                } else {
                    row.style.display = 'none';
                }
            });

            if (!foundMahasiswa) {
                noDataMessageMahasiswa.style.display = 'block';
            } else {
                noDataMessageMahasiswa.style.display = 'none';
            }
        });
    });
    document.addEventListener("DOMContentLoaded", function() {
        const searchInputDosen = document.getElementById('searchInputDosen');
        const tableRowsDosen = document.querySelectorAll('#modalDosen table tbody tr');
        const noDataMessageDosen = document.getElementById('noDataMessageDosen');

        searchInputDosen.addEventListener('input', function() {
            const searchTermDosen = searchInputDosen.value.trim().toLowerCase();
            let foundDosen = false;

            tableRowsDosen.forEach(function(row) {
                const cells = row.querySelectorAll('td');
                let rowFound = false;

                cells.forEach(function(cell) {
                    const cellText = cell.textContent.toLowerCase();
                    if (cellText.includes(searchTermDosen)) {
                        rowFound = true;
                        foundDosen = true;
                    }
                });

                if (rowFound) {
                    row.style.display = '';
                } else {
                    row.style.display = 'none';
                }
            });

            if (!foundDosen) {
                noDataMessageDosen.style.display = 'block';
            } else {
                noDataMessageDosen.style.display = 'none';
            }
        });
    });
    </script>


</div>

<div>
    <div class="card shadow-lg mx-4 mt-5 card-profile-bottom">
        <div class="card-body p-3 mt-3">
            @component('components.table.table-header', [
                'title' => 'Input Jadwal Prasidang',
                'button_title' => __(''),
                'with_sort' => false,
                'is_function' => false,
                'with_filter' => false,
                'with_entries' => false,
                'with_search' => false,
                'click' => route('admin.dashboard'),
                'with_modal' => false,
                'with_button' => false,
            ])
            @endcomponent
        </div>
    </div>
    <div class="container py-4">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        <div class="row gx-4">
                            <div class="card shadow">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-3 mb-3 text-dark font-weight-bold">Periode</div>
                                        <div class="col-sm-9 text-sm text-dark font-weight-bold">
                                            : {{ $mahasiswaSidang->tahun_ajaran }} |
                                            {{ $mahasiswaSidang->semester }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 mb-3 text-dark font-weight-bold">Nama Mahasiswa</div>
                                        <div class="col-sm-9 text-sm text-dark font-weight-bold">
                                            : {{ $mahasiswaSidang->mahasiswa->nama_mahasiswa }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 mb-3 text-dark font-weight-bold">Judul PA</div>
                                        <div class="col-sm-9 text-sm text-dark font-weight-bold">
                                            : {{ $mahasiswaSidang->judul_indo }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 mb-3 text-dark font-weight-bold">Title</div>
                                        <div class="col-sm-9 text-sm text-dark font-weight-bold">
                                            : {{ $mahasiswaSidang->judul_inggris }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 mb-3 text-dark font-weight-bold">Pembimbing 1</div>
                                        <div class="col-sm-9 text-sm text-dark font-weight-bold">
                                            :  {{ $mahasiswaSidang->pembimbing1->nama_dosen }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 mb-3 text-dark font-weight-bold">Pembimbing 2</div>
                                        <div class="col-sm-9 text-sm text-dark font-weight-bold">
                                            :  {{ $mahasiswaSidang->pembimbing2->nama_dosen }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 mb-3 text-dark font-weight-bold">Penguji 1</div>
                                        <div class="col-sm-9 text-sm text-dark font-weight-bold">
                                            :  {{ $mahasiswaSidang->penguji1->nama_dosen }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 mb-3 text-dark font-weight-bold">Penguji 2</div>
                                        <div class="col-sm-9 text-sm text-dark font-weight-bold">
                                            :  {{ $mahasiswaSidang->penguji2->nama_dosen }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col-md-6 mb-3">
                                <div for="periode" class="form-label">Periode</div>
                                <input type="text" class="form-control" id="periode" wire:model="periode">
                            </div>
                            <div class="col-md-6 mb-3">
                                <div for="bulan" class="form-label">Bulan</div>
                                <input type="text" class="form-control" id="bulan" wire:model="bulan">
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col-md-6 mb-3">
                                <div for="tanggal" class="form-label">Tanggal</div>
                                <input type="date" class="form-control" id="tanggal" wire:model="tanggal">
                            </div>
                            <div class="col-md-3 mb-3">
                                <div for="jam" class="form-label">Jam mulai</div>
                                <input type="time" class="form-control" id="jam" wire:model="jam_mulai">
                            </div>
                            <div class="col-md-3 mb-3">
                                <div for="jam" class="form-label">Jam selesai</div>
                                <input type="time" class="form-control" id="jam" wire:model="jam_mulai">
                            </div>
                        </div>
                        <button wire:click="createData" type="submit" class="btn btn-primary btn-save shadow bg-primary">Simpan</button>
                    </div>
                </div>
            </div>
            </section>
        </div>
    </div>
    </div>
</div>

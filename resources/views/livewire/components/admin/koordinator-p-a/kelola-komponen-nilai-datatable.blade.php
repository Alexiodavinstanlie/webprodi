<div>
    <div class="card shadow-lg mx-4 mt-5 card-profile-bottom">
        <div class="card-body p-3 mt-3">
            @component('components.table.table-header', [
                'title' => 'Input Komponen Nilai',
                'button_title' => __(''),
                'with_sort' => false,
                'is_function' => false,
                'with_filter' => false,
                'with_entries' => false,
                'with_search' => false,
                'click' => route('admin.dashboard'),
                'with_modal' => false,
                'with_button' => false,
            ])
            @endcomponent
        </div>
    </div>
    <div class="container py-4">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        <div class="row gx-4">
                            <div class="card shadow">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-3 mb-3 text-dark font-weight-bold">Kode Koordinator</div>
                                        <div class="col-sm-9 text-sm text-dark font-weight-bold">
                                            : {{ auth()->user()->dosen->singkatan_dosen }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 mb-3 text-dark font-weight-bold">Nama Koordinator PA</div>
                                        <div class="col-sm-9 text-sm text-dark font-weight-bold">
                                            : {{ auth()->user()->dosen->nama_dosen }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 mb-3 text-dark font-weight-bold">Program Studi</div>
                                        <div class="col-sm-9 text-sm text-dark font-weight-bold">
                                            : D3 Sistem Informasi
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 mb-3 text-dark font-weight-bold">Semester</div>
                                        <div class="col-sm-9 text-sm text-dark font-weight-bold">
                                            : {{ $currentSemester->semester }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 mb-3 text-dark font-weight-bold">Tahun Ajaran</div>
                                        <div class="col-sm-9 text-sm text-dark font-weight-bold">
                                            : {{ $currentSemester->tahun_ajaran }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <section class="content mb-4 mt-4">
                                <div class="row">
                                    <div class="col-3">
                                        <div class="nav-wrapper position-relative end-0">
                                            <ul class="nav nav-pills nav-fill flex-column p-1" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link mb-0 px-0 py-1 active" data-bs-toggle="tab"
                                                        href="#profile-tabs-vertical" role="tab"
                                                        aria-controls="preview" aria-selected="true">
                                                        Proposal
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link mb-0 px-0 py-1" data-bs-toggle="tab"
                                                        href="#dashboard-tabs-vertical" role="tab"
                                                        aria-controls="code" aria-selected="false">
                                                        Prasidang
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link mb-0 px-0 py-1" data-bs-toggle="tab"
                                                        href="#payments-tabs-vertical" role="tab"
                                                        aria-controls="code" aria-selected="false">
                                                        Sidang
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-9">
                                        <div class="card shadow">
                                            <div class="card-body">
                                                <div class="tab-content" id="myTabContent">
                                                    <div class="tab-pane fade show active" id="profile-tabs-vertical"
                                                        role="tabpanel" aria-labelledby="profile-tabs-vertical-tab">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <label
                                                                    class="col-form-label text-sm font-weight-bold">Periode</label>
                                                                <input type="hidden" name="periode_id"
                                                                    class="form-control" id="periode_id" value="">
                                                                <input type="text" class="form-control"
                                                                    value="{{ $currentSemester->tahun_ajaran }} | {{ $currentSemester->semester }}"
                                                                    readonly>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <label
                                                                    class="col-form-label text-sm font-weight-bold">Tanggal
                                                                    Deadline
                                                                    Input Nilai
                                                                </label>
                                                                <input type="date"
                                                                    name="tanggal_deadline_input_nilai"
                                                                    class="form-control"
                                                                    value="{{ $deadline_proposal->deadline ?? '' }}"
                                                                    required>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="col-lg-12 border rounded mx-auto table-responsive mt-4">
                                                            <table class="table align-items-center mb-0">
                                                                <thead>
                                                                    <tr>
                                                                        <th
                                                                            class="text-uppercase text-dark text-xs font-weight-bolder">
                                                                            NO KN</th>
                                                                        <th
                                                                            class="text-uppercase text-dark text-xs font-weight-bolder ps-2">
                                                                            Nama Komponen</th>
                                                                        <th
                                                                            class="text-center text-uppercase text-dark text-xs font-weight-bolder ">
                                                                            Nilai Maksimal</th>
                                                                        <th
                                                                            class="text-center text-uppercase text-dark text-xs font-weight-bolder ">
                                                                            Aksi</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach ($komponen_nilais as $item)
                                                                        <tr>
                                                                            <td>
                                                                                <div class="d-flex px-2 py-1">
                                                                                    <div
                                                                                        class="d-flex flex-column justify-content-center">
                                                                                        <h6
                                                                                            class="mb-0 text-sm ms-auto">
                                                                                            KN
                                                                                            {{ $nomorKN++ }}</h6>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <p
                                                                                    class="text-sm font-weight-bold mb-0">
                                                                                    {{ $item->nama }}
                                                                                </p>
                                                                            </td>
                                                                            <td
                                                                                class="align-middle text-center text-sm">
                                                                                <span
                                                                                    class="text-sm font-weight-bold mb-0">{{ $item->nilai_komponen }}</span>
                                                                            </td>
                                                                            <td class="align-middle text-center">
                                                                                <a wire:click="showModal('edit', {{ $item->id }})"
                                                                                    class="btn btn-xs btn-info mt-2" data-bs-toggle="modal"
                                                                                    data-bs-target="#nilaimutuModal">
                                                                                    Edit
                                                                                </a>
                                                                                <button type="button"
                                                                                    class="btn btn-xs btn-danger mt-2">Hapus</button>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                        <button type="button" class="btn btn-primary mt-4"
                                                            data-bs-toggle="modal" data-bs-target="#tambahNilaiModal">+
                                                            Tambah Nilai</button>

                                                        <div class="modal fade" id="tambahNilaiModal" tabindex="-1"
                                                            aria-labelledby="exampleModalLabel" aria-hidden="true"
                                                            wire:ignore.self>
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">
                                                                            @if ($mode == 'create')
                                                                            Tambah Nilai
                                                                        @else
                                                                            Edit Nilai
                                                                        @endif
                                                                        </h5>
                                                                        <button type="button" class="btn-close"
                                                                            data-bs-dismiss="modal"
                                                                            aria-label="Close"></button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form>
                                                                            <div class="mb-3">
                                                                                <label for="nomor_kn"
                                                                                    class="form-label">No
                                                                                    KN</label>
                                                                                <input type="text"
                                                                                    wire:model="nomorKN"
                                                                                    class="form-control"
                                                                                    id="nomor_kn" disabled>
                                                                            </div>
                                                                            <div class="mb-3">
                                                                                <label for="nama"
                                                                                    class="form-label">Nama</label>
                                                                                <input type="text"
                                                                                    wire:model="nama"
                                                                                    class="form-control"
                                                                                    id="nama"
                                                                                    placeholder="Masukkan Nama">
                                                                            </div>
                                                                            <div class="mb-3">
                                                                                <label for="nilai_komponen"
                                                                                    class="form-label">Nilai</label>
                                                                                <input type="number"
                                                                                    wire:model="nilai_komponen"
                                                                                    class="form-control"
                                                                                    id="nilai_komponen"
                                                                                    placeholder="Masukkan Nilai">
                                                                            </div>
                                                                            @if ($mode == 'create')
                                                                                <button type="submit" class="btn btn-primary" wire:click="createComponent">Simpan</button>
                                                                            @else
                                                                                <button type="submit" class="btn btn-primary" wire:click="editComponent">Update</button>
                                                                            @endif
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="dashboard-tabs-vertical"
                                                        role="tabpanel" aria-labelledby="dashboard-tabs-vertical-tab">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <label
                                                                    class="col-form-label text-sm font-weight-bold">Periode</label>
                                                                <input type="hidden" name="periode_id"
                                                                    class="form-control" id="periode_id"
                                                                    value="">
                                                                <input type="text" class="form-control"
                                                                    value="{{ $currentSemester->tahun_ajaran }} | {{ $currentSemester->semester }}" readonly>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <label
                                                                    class="col-form-label text-sm font-weight-bold">Tanggal
                                                                    Deadline
                                                                    Input Nilai
                                                                </label>
                                                                <input type="date"
                                                                    name="tanggal_deadline_input_nilai"
                                                                    class="form-control"
                                                                    value="{{ $deadline_proposal->deadline ?? '' }}"
                                                                    required>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="col-lg-12 border rounded mx-auto table-responsive mt-4">
                                                            <table class="table align-items-center mb-0">
                                                                <thead>
                                                                    <tr>
                                                                        <th
                                                                            class="text-uppercase text-dark text-xs font-weight-bolder">
                                                                            NO KN</th>
                                                                        <th
                                                                            class="text-uppercase text-dark text-xs font-weight-bolder ps-2">
                                                                            Nama Komponen</th>
                                                                        <th
                                                                            class="text-center text-uppercase text-dark text-xs font-weight-bolder ">
                                                                            Nilai Maksimal</th>

                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach ($komponen_nilai_prasidangs as $item)
                                                                        <tr>
                                                                            <td>
                                                                                <div class="d-flex px-2 py-1">
                                                                                    <div
                                                                                        class="d-flex flex-column justify-content-center">
                                                                                        <h6
                                                                                            class="mb-0 text-sm ms-auto">
                                                                                            KN
                                                                                            {{ $nomorKNPrasidang++ }}
                                                                                        </h6>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <p
                                                                                    class="text-sm font-weight-bold mb-0">
                                                                                    {{ $item->nama_prasidang }}
                                                                                </p>
                                                                            </td>
                                                                            <td
                                                                                class="align-middle text-center text-sm">
                                                                                <span
                                                                                    class="text-sm font-weight-bold mb-0">{{ $item->nilai_komponen_prasidang }}</span>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                        <button type="button" class="btn btn-primary mt-4"
                                                            data-bs-toggle="modal"
                                                            data-bs-target="#tambahNilaiPrasidangModal">+ Tambah
                                                            Nilai</button>

                                                        <button type="button" class="btn btn-primary mt-4"
                                                            data-bs-toggle="modal"
                                                            data-bs-target="#tambahNilaiPrasidangModal">Edit
                                                            Nilai</button>

                                                        <div class="modal fade" id="tambahNilaiPrasidangModal"
                                                            tabindex="-1" aria-labelledby="exampleModalLabel"
                                                            aria-hidden="true" wire:ignore.self>
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title"
                                                                            id="exampleModalLabel">Tambah Nilai
                                                                            Prasidang
                                                                        </h5>
                                                                        <button type="button" class="btn-close"
                                                                            data-bs-dismiss="modal"
                                                                            aria-label="Close"></button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form
                                                                            wire:submit.prevent="createComponentPrasidang">
                                                                            <div class="mb-3">
                                                                                <label for="nomor_kn"
                                                                                    class="form-label">No
                                                                                    KN</label>
                                                                                <input type="text"
                                                                                    wire:model="nomorKN"
                                                                                    class="form-control"
                                                                                    id="nomor_kn" disabled>
                                                                            </div>
                                                                            <div class="mb-3">
                                                                                <label for="nama"
                                                                                    class="form-label">Nama</label>
                                                                                <input type="text"
                                                                                    wire:model="nama_prasidang"
                                                                                    class="form-control"
                                                                                    id="nama"
                                                                                    placeholder="Masukkan Nama">
                                                                            </div>
                                                                            <div class="mb-3">
                                                                                <label for="nilai_komponen_prasidang"
                                                                                    class="form-label">Nilai</label>
                                                                                <input type="number"
                                                                                    wire:model="nilai_komponen_prasidang"
                                                                                    class="form-control"
                                                                                    id="nilai_komponen_prasidang"
                                                                                    placeholder="Masukkan Nilai">
                                                                            </div>
                                                                            <button type="submit"
                                                                                class="btn btn-primary">Simpan</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="payments-tabs-vertical"
                                                        role="tabpanel" aria-labelledby="payments-tabs-vertical-tab">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <label
                                                                    class="col-form-label text-sm font-weight-bold">Periode</label>
                                                                <input type="hidden" name="periode_id"
                                                                    class="form-control" id="periode_id"
                                                                    value="">
                                                                <input type="text" class="form-control"
                                                                    value="{{ $currentSemester->tahun_ajaran }} | {{ $currentSemester->semester }}" readonly>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <label
                                                                    class="col-form-label text-sm font-weight-bold">Tanggal
                                                                    Deadline
                                                                    Input Nilai
                                                                </label>
                                                                <input type="date"
                                                                    name="tanggal_deadline_input_nilai"
                                                                    class="form-control"
                                                                    value="{{ $deadline_proposal->deadline ?? '' }}"
                                                                    required>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="col-lg-12 border rounded mx-auto table-responsive mt-4">
                                                            <table class="table align-items-center mb-0">
                                                                <thead>
                                                                    <tr>
                                                                        <th
                                                                            class="text-uppercase text-dark text-xs font-weight-bolder">
                                                                            NO KN</th>
                                                                        <th
                                                                            class="text-uppercase text-dark text-xs font-weight-bolder ps-2">
                                                                            Nama Komponen</th>
                                                                        <th
                                                                            class="text-center text-uppercase text-dark text-xs font-weight-bolder ">
                                                                            Nilai Maksimal</th>

                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach ($komponen_nilai_sidangs as $item)
                                                                        <tr>
                                                                            <td>
                                                                                <div class="d-flex px-2 py-1">
                                                                                    <div
                                                                                        class="d-flex flex-column justify-content-center">
                                                                                        <h6
                                                                                            class="mb-0 text-sm ms-auto">
                                                                                            KN
                                                                                            {{ $nomorKNSidang++ }}</h6>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <p
                                                                                    class="text-sm font-weight-bold mb-0">
                                                                                    {{ $item->nama_sidang }}
                                                                                </p>
                                                                            </td>
                                                                            <td
                                                                                class="align-middle text-center text-sm">
                                                                                <span
                                                                                    class="text-sm font-weight-bold mb-0">{{ $item->nilai_komponen_sidang }}</span>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                        <button type="button" class="btn btn-primary mt-4"
                                                            data-bs-toggle="modal"
                                                            data-bs-target="#tambahNilaiSidangModal">+ Tambah
                                                            Nilai</button>

                                                        <button type="button" class="btn btn-primary mt-4"
                                                            data-bs-toggle="modal"
                                                            data-bs-target="#tambahNilaiSidangModal">Edit
                                                            Nilai</button>

                                                        <div class="modal fade" id="tambahNilaiSidangModal"
                                                            tabindex="-1" aria-labelledby="exampleModalLabel"
                                                            aria-hidden="true" wire:ignore.self>
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title"
                                                                            id="exampleModalLabel">Tambah Nilai
                                                                        </h5>
                                                                        <button type="button" class="btn-close"
                                                                            data-bs-dismiss="modal"
                                                                            aria-label="Close"></button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form
                                                                            wire:submit.prevent="createComponentSidang">
                                                                            <div class="mb-3">
                                                                                <label for="nomor_kn"
                                                                                    class="form-label">No
                                                                                    KN</label>
                                                                                <input type="text"
                                                                                    wire:model="nomorKN"
                                                                                    class="form-control"
                                                                                    id="nomor_kn" disabled>
                                                                            </div>
                                                                            <div class="mb-3">
                                                                                <label for="nama"
                                                                                    class="form-label">Nama</label>
                                                                                <input type="text"
                                                                                    wire:model="nama_sidang"
                                                                                    class="form-control"
                                                                                    id="nama"
                                                                                    placeholder="Masukkan Nama">
                                                                            </div>
                                                                            <div class="mb-3">
                                                                                <label for="nilai_komponen"
                                                                                    class="form-label">Nilai</label>
                                                                                <input type="number"
                                                                                    wire:model="nilai_komponen_sidang"
                                                                                    class="form-control"
                                                                                    id="nilai_komponen"
                                                                                    placeholder="Masukkan Nilai">
                                                                            </div>
                                                                            <button type="submit"
                                                                                class="btn btn-primary">Simpan</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
            </section>
        </div>
    </div>

<div>
    <div class="col-8 mx-auto">
        <div class="card shadow-lg mx-4 mt-5 card-profile-bottom">
            <div class="card-body p-3">
                @component('components.table.table-header', [
                    'title' => 'Tahun Ajaran Aktif',
                    'with_sort' => false,
                    'is_function' => false,
                    'with_filter' => false,
                    'with_button' => false,
                ])
                    <button wire:click="showModal('create')" class="btn bg-gradient-danger btn-sm w-100 px-3" type="button" data-bs-toggle="modal"
                        data-bs-target="#tahunajaranModal">+&nbsp;
                        <span class="d-none d-md-inline">
                            Tambah Tahun Ajaran
                        </span>
                    </button>
                @endcomponent
            </div>
        </div>
    </div>
    <div class="container py-4">
        <div class="row">
            <div class="col-8 mx-auto">
                <div class="card">
                    <div class="card shadow-lg">
                        <div class="card-body p-3">
                            <div class="row mt-2">
                                <div class="col-sm-5">
                                    <input wire:model="tahun_ajaran" type="text" name="tahun_ajaran_active" class="form-control rounded" id="tahun_ajaran_active" value="{{ $tahun_ajaran_awal_aktif }} - {{ $tahun_ajaran_akhir_aktif }}" readonly>
                                </div>
                                <div class="col-sm-4">
                                    <select wire:model="semester" name="semester" class="form-control rounded" required>
                                        <option value="">Pilih Semester</option>
                                        <option value="Ganjil" {{ $semester === 'Ganjil' ? 'selected' : '' }}>Ganjil</option>
                                        <option value="Genap" {{ $semester === 'Genap' ? 'selected' : '' }}>Genap</option>
                                    </select>
                                </div>
                                <div class="col-sm-3 mt-1">
                                    <button wire:click="submitsemester" type="submit" class="btn btn-sm btn-primary">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container py-4">
        <div class="row">
            <div class="col-8 mx-auto">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        <div class="row gx-4">
                            <div class="card">
                                <div class="table-responsive">
                                    <table class="table align-items-center mb-0">
                                        <thead>
                                            <tr>
                                                <th
                                                    class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    NO</th>
                                                <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    Tahun Ajaran</th>
                                                <th
                                                    class="text-uppercase text-secondary text-center text-xxs font-weight-bolder opacity-7 ps-2">
                                                    Status</th>
                                                <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($tahunAjarans as $item)
                                            <tr>
                                                <td>
                                                    <div class="d-flex px-2 py-1">
                                                        <div class="d-flex flex-column justify-content-center">
                                                            <h6 class="mb-0 text-xs">{{ $num++ }}</h6>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <p class="text-center text-xs font-weight-bold mb-0">
                                                        {{ $item->tahun_ajaran_awal }} - {{ $item->tahun_ajaran_akhir }}
                                                    </p>
                                                </td>
                                            </td>
                                            <td class="align-middle text-center ">
                                                <div class="d-flex justify-content-center"
                                                wire:loading.attr="disabled">
                                                <div class="form-check form-switch">
                                                    <input class="form-check-input" type="checkbox"
                                                        wire:change="isActive({{ $item->id }})"
                                                        {{ $tahunAjaranCount == $maxTahunAjaranCount && !$item->is_active ? 'disabled' : '' }}
                                                        {{ $item->is_active ? 'checked' : '' }}>
                                                    <label class="form-check-label"
                                                        for="isActive"></label>
                                                </div>
                                            </td>
                                                <td class="align-middle text-center">
                                                    <button type="button"
                                                        class="btn btn-xs btn-danger mt-2">Hapus</button>
                                                </td>
                                            </tr>
                                            @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {{-- @component('components.table.table-footer', [
                                'datas' => $tahunAjarans,
                            ])
                            @endcomponent --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div wire:ignore.self class="modal tahunajaran fade" id="tahunajaranModal" tabindex="-1" role="dialog"
            aria-labelledby="tahunajaranmodallabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="tahunajaranmodallabel">
                            {{-- @if ($mode == 'create')
                            Tambah Tahun Ajaran
                        @else
                            Edit Tahun Ajaran
                        @endif
                        </h5> --}}
                    </div>
                    <div class="modal-body">
                        <div>
                            <form>
                                <form>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="tahun_ajaran_awal">Tahun Ajaran awal:</label>
                                                <input type="text" wire:model="tahun_ajaran_awal" class="form-control">
                                                @error('tahun_ajaran_awal')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="tahun_ajaran_akhir">Tahun Ajaran Akhir:</label>
                                                <input type="text" wire:model="tahun_ajaran_akhir" class="form-control">
                                                @error('tahun_ajaran_akhir')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <button wire:click="createData"type="submit" class="btn btn-primary">Simpan</button>
                                </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div>
    <div class="card shadow-lg mx-4 mt-5 card-profile-bottom">
        <div class="card-body p-3 mt-3">
            @component('components.table.table-header', [
                'title' => 'Jadwal Prasidang',
                'button_title' => __(''),
                'with_sort' => false,
                'is_function' => false,
                'with_filter' => true,
                'with_entries' => true,
                'with_search' => true,
                'click' => route('admin.dashboard'),
                'with_modal' => false,
                'with_button' => false,
            ])
            @endcomponent
        </div>
    </div>
    <div class="container py-4">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        <div class="row gx-4">
                            <div class="card">
                                <div class="table-responsive">
                                    <table class="table align-items-center mb-0">
                                        <thead>
                                            <tr>
                                                <th
                                                    class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    NO</th>
                                                <th
                                                    class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                    NIM</th>
                                                <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    Nama Mahasiswa</th>
                                                <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    Judul</th>
                                                <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    Title</th>
                                                <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    PBB 1</th>
                                                <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    PBB 2</th>
                                                <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    PUJ 1</th>
                                                <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    PUJ 2</th>
                                                <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    Periode</th>
                                                <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    Bulan</th>
                                                <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    Tanggal Prasidang</th>
                                                    <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    Jam Mulai</th>
                                                    <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    Jam Selesai</th>
                                                    <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    Ruangan</th>
                                                    <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    Link</th>
                                                    <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {{-- @foreach ($periodes as $periode)
                                                <tr>
                                                    <td>
                                                        <div class="d-flex px-2 py-1">
                                                            <div class="d-flex flex-column justify-content-center">
                                                                <h6 class="mb-0 text-xs">{{ $num++ }}</h6>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <p class="text-xs font-weight-bold mb-0">{{ $periode->periode }}
                                                        </p>
                                                    </td>
                                                    <td class="align-middle text-center text-sm">
                                                        <span
                                                            class="badge badge-sm badge-success">{{ $periode->semester }}</span>
                                                    </td>
                                                    <td class="align-middle text-center">
                                                        <span
                                                            class="text-secondary text-xs font-weight-bold">{{ $periode->bulan }}</span>
                                                    </td>
                                                    <td class="align-middle text-center">
                                                        <span
                                                            class="text-secondary text-xs font-weight-bold">{{ $periode->tahun }}</span>
                                                    </td>
                                                    <td class="align-middle text-center">
                                                        <a href="{{ route('periode.manage', ['id_periode' => $periode->id]) }}"
                                                            class="btn btn-xs btn-info">Edit</a>
                                                            <a wire:click="selectCommand('delete','{{ $periode->id }}')" class="btn btn-xs btn-danger">Hapus
                                                            </a>
                                                        </td>
                                                </tr>
                                            @endforeach --}}
                                        </tbody>
                                    </table>
                                    {{-- @component('components.table.table-footer', [
                                        'datas' => $periodes,
                                    ])
                                    @endcomponent --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
        <div class="offcanvas-header">
            <h5 id="offcanvasRightLabel" class="mb-0">Filter</h5>
            <button type="button" class="btn-close text-dark" data-bs-dismiss="offcanvas" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="offcanvas-body">
            <div class="col-12 col-sm-12 mt-3">
                <label>Semester</label>
                <select wire:model="semester" class="form-control">
                    {{-- <option value="">Pilih Semester</option>
                    @foreach ($periodes as $semester)
                    <option value="{{ $semester->semester }}">{{ $semester->semester }}</option>
                @endforeach --}}
                </select>
            </div>
            <div class="col-12 mt-3">
                <button wire:click="applyFilter" class="btn btn-primary">Terapkan Filter</button>
            </div>
        </div>
    </div>
</div>

<div>
    <div>
        <div class="card shadow-lg mx-4 mt-5 card-profile-bottom">
            <div class="card-body p-3 mt-3">
                @component('components.table.table-header', [
                    'title' => 'Kaprodi',
                    'button_title' => __('Kaprodi'),
                    'with_sort' => false,
                    'is_function' => false,
                    'with_filter' => false,
                    'with_entries' =>false,
                    'with_search' =>false,
                    'click' => route('admin.dashboard')
                ])
                @endcomponent
            </div>
        </div>
        <div class="container py-4">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header">
                            <div class="row gx-4">
                                <div class="card">
                                    <div class="table-responsive">
                                        <table class="table align-items-center mb-0">
                                            <thead>
                                                <tr>
                                                    <th
                                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                        NO</th>
                                                    <th
                                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                        Nama Dosen</th>
                                                    <th
                                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                        Kode Dosen</th>
                                                    <th
                                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                        NIP</th>
                                                    <th
                                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                        Menjabat</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="d-flex px-2 py-1">
                                                            <div class="d-flex flex-column justify-content-center">
                                                                <h6 class="mb-0 text-xs">1</h6>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <p class="text-xs font-weight-bold mb-0">Patrick Adolf Telnoni, S.T., M.T.
                                                        </p>
                                                    </td>
                                                    <td class="align-middle text-center text-lg">
                                                        <span class="badge badge-sm badge-success">PTI</span>
                                                    </td>
                                                    <td class="align-middle text-center">
                                                        <span
                                                            class="text-secondary text-xs font-weight-bold">5171</span>
                                                    </td>
                                                    <td class="align-middle text-center">
                                                        <div class="d-flex justify-content-center">
                                                            <div class="form-check form-switch">
                                                                <input class="form-check-input" type="checkbox" id="toggleOption">
                                                                <label class="form-check-label" for="toggleOption"></label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

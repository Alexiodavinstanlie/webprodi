<div>
    <div>
        <div class="card shadow-lg mx-4 mt-5 card-profile-bottom">
            <div class="card-body p-3">
                @component('components.table.table-header', [
                    'title' => 'Role Dosen',
                    'button_title' => __('Role'),
                    'with_sort' => false,
                    'is_function' => false,
                    'with_filter' => true,
                    'click' => route('admin.mahasiswa.create'),
                    'with_modal' => false,
                ])
                @endcomponent
            </div>
        </div>
        <div class="container py-4">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header">
                            <div class="row gx-4">
                                <div class="card">
                                    <div class="table-responsive">
                                        <table class="table align-items-center mb-0">
                                            <thead>
                                                <tr>
                                                    <th
                                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                        NO</th>
                                                    <th
                                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                        Kode Role</th>
                                                    <th
                                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                        Role</th>
                                                    <th
                                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                        Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="d-flex px-2 py-1">
                                                            <div class="d-flex flex-column justify-content-center">
                                                                <h6 class="mb-0 text-xs">1</h6>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <p class="text-xs font-weight-bold mb-0">Patrick Adolf Telnoni, S.T., M.T.
                                                        </p>
                                                    </td>
                                                    <td class="align-middle text-center text-lg">
                                                        <span class="badge badge-sm badge-success">PTI</span>
                                                    </td>
                                                    <td class="align-middle text-center">
                                                        <button type="button"
                                                            class="btn btn-xs btn-info mt-2">edit</button>
                                                        <button type="button"
                                                            class="btn btn-xs btn-danger mt-2">Hapus</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


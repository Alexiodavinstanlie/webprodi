<div class="container mt-6">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title mb-5">{{ $periode_data ? 'Edit' : 'Input' }} Periode</h5>

                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="periode" class="form-label">Tahun Ajaran</label>
                        <input wire:model="periode" type="text" class="form-control" id="periode" >
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="tahun" class="form-label">Tahun</label>
                        <input type="text" class="form-control" id="tahun" wire:model="tahun">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="semester" class="form-label">Semester</label>
                        <select class="form-select" id="semester" wire:model="semester">
                            <option value="">Pilih Semester</option>
                            <option value="Genap">Genap</option>
                            <option value="Ganjil">Ganjil</option>
                        </select>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="bulan" class="form-label">Bulan</label>
                        <select class="form-select" id="bulan" wire:model="bulan">
                            <option value="">Pilih Bulan</option>
                            @foreach($months as $month)
                                <option value="{{ ucfirst($month) }}">{{ ucfirst($month) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <button wire:click="{{ $periode_data ? 'updatePeriode' : 'createPeriode' }}" type="submit" class="btn btn-primary">{{ $periode_data ? 'Update' : 'Simpan' }}</button>
        </div>
    </div>
</div>

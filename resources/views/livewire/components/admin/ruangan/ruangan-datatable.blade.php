<div>
    <div>
        <div class="card shadow-lg mx-4 mt-5 card-profile-bottom">
            <div class="card-body p-3">
                @component('components.table.table-header', [
                    'title' => 'Data Ruangan',
                    'button_title' => __('Ruangan'),
                    'with_sort' => false,
                    'is_function' => false,
                    'with_filter' => true,
                    'click' => route('admin.mahasiswa.create'),
                    'with_modal' => false,
                ])
                @endcomponent
            </div>
        </div>
        <div class="container py-4">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header">
                            <div class="row gx-4">
                                <div class="tab-content">
                                    <div id="sidang" class="tab-pane fade show active">
                                        <div class="card">
                                            <div class="table-responsive">
                                                <table class="table align-items-center mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th
                                                                class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                                NO</th>
                                                            <th
                                                                class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                                Kode Ruangan</th>
                                                            <th
                                                                class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                                Ruangan</th>
                                                            <th
                                                                class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                                Keterangan</th>
                                                            <th
                                                                class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                                Status</th>
                                                            <th
                                                                class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                                Aksi</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                        $num = ($ruangans->currentPage() - 1) * $ruangans->perPage() + 1;
                                                        @endphp
                                                        @foreach ($ruangans as $ruangan )
                                                        <tr>
                                                            <td>
                                                                <div class="d-flex px-2 py-1">
                                                                    <div
                                                                        class="d-flex flex-column justify-content-center">
                                                                        <h6 class="mb-0 text-xs">{{ $num++}}</h6>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <p class="text-xs font-weight-bold mb-0">{{ $ruangan->kode_ruangan }}
                                                                </p>
                                                            </td>
                                                            <td class="align-middle text-center text-lg">
                                                                <span class="text-secondary text-xs font-weight-bold">{{ $ruangan->nama_ruangan }}</span>
                                                            </td>
                                                            <td class="align-middle text-center">
                                                                <span
                                                                    class="text-secondary text-xs font-weight-bold">{{ $ruangan->keterangan_ruangan }}</span>
                                                            </td>
                                                            <td class="align-middle text-center">
                                                                <span class="badge badge-sm badge-success">{{ $ruangan->status }}</span>
                                                            </td>
                                                            <td class="align-middle text-center">
                                                                <button type="button"
                                                                    class="btn btn-xs btn-info mt-2">edit</button>
                                                                <button type="button"
                                                                    class="btn btn-xs btn-danger mt-2">Hapus</button>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                @component('components.table.table-footer', [
                                                    'datas' => $ruangans,
                                                ])
                                                @endcomponent
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

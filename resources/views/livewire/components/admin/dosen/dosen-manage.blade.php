<div>
    <div class="container mt-6">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title mb-5">Input Daftar Dosen</h5>
                <form>
                    <div class="mt-3">
                        <button wire:click="exportTemplate" class="btn btn-icon btn-3 btn-primary btn-sm" type="button" wire:loading.attr="disabled" style="margin-right: 10px;">
                            <div wire:loading wire:target="exportTemplate">
                                <div class="spinner-border text-light spinner-border-sm" role="status"></div>
                            </div>
                            <span class="btn-inner-text"> Download template excel</span>
                        </button>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="nama_prodi" class="form-label">Nama Prodi</label>
                            <input type="text" class="form-control" id="nama_prodi" wire:model="nama_prodi" disabled>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="singkatan_prodi" class="form-label">Singkatan Prodi</label>
                            <input type="text" class="form-control" id="singkatan_prodi" wire:model="prodi"
                                disabled>
                        </div>
                    </div>
            <div class="mb-3">
                <div class="col-12 col-sm-12 mt-3 mt-sm-0 {{ $file ? 'd-none' : '' }}">
                    <label class="control-label">Unggah File</label>
                    <input wire:model="file" type="file" id="file-{{ $iteration_file }}" class="form-control @error('file') is-invalid @enderror">
                    @error('file') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
                <div class="col-12 col-sm-12 mt-3 mt-sm-0 {{ $file ? '' : 'd-none' }}">
                    <label class="control-label">Unggah File</label>
                    <div class="d-flex justify-content-between align-items-center border rounded p-2">
                        <div class="col-5">
                            <i class="fa fa-check text-success"></i><span style="font-size: 12px"> Data Berhasil Diperbarui</span>
                        </div>
                        <div class="col-1">
                            <a wire:click="clearFile" type="button"><i class="fa fa-close text-danger"></i></a>
                        </div>
                    </div>
                    @error('logo') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
            </div>
            <button wire:click="createDosen" type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>
    </div>
</div>
</div>

<div>
    <div>
        <div class="card shadow-lg mx-4 mt-5 card-profile-bottom">
            <div class="card-body p-3">
                @component('components.table.table-header', [
                    'title' => 'Data Dosen',
                    'button_title' => __('Data Dosen'),
                    'with_sort' => false,
                    'is_function' => false,
                    'with_filter' => true,
                    'click' => route('admin.dosen.manage'),
                    'with_modal' => false,
                ])
                @endcomponent
            </div>
        </div>
        <div class="container py-4">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header">
                            <div class="row gx-4">
                                <div class="card">
                                    <div class="table-responsive">
                                        <table class="table align-items-center mb-0">
                                            <thead>
                                                <tr>
                                                    <th
                                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                        NO</th>
                                                    <th
                                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                        Nama Dosen</th>
                                                    <th
                                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                        Kode Dosen</th>
                                                    <th
                                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                        NIP</th>
                                                    <th
                                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                        Prodi</th>
                                                    <th
                                                        class="text-start text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                        PBB </th>
                                                    <th
                                                        class="text-start text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                        PUJ </th>
                                                    <th
                                                        class="text-start text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                        WalDos </th>
                                                    <th
                                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                        Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($dosens as $dosen)
                                                    <tr>
                                                        <td>
                                                            <div class="d-flex px-2 py-1">
                                                                <div class="d-flex flex-column justify-content-center">
                                                                    <h6 class="mb-0 text-xs">{{ $num++ }}</h6>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <p class="text-xs font-weight-bold mb-0">
                                                                {{ $dosen->nama_dosen }}
                                                            </p>
                                                        </td>
                                                        <td class="align-middle text-center text-lg">
                                                            <span
                                                                class="badge badge-sm badge-success">{{ $dosen->singkatan_dosen }}</span>
                                                        </td>
                                                        <td class="align-middle text-center">
                                                            <span
                                                                class="text-secondary text-xs font-weight-bold">{{ $dosen->nip }}</span>
                                                        </td>
                                                        <td class="align-middle text-center">
                                                            <span
                                                                class="text-secondary text-xs font-weight-bold">{{ $dosen->prodi }}
                                                            </span>
                                                        </td>
                                                        <td class="align-middle text-center">
                                                            <div class="form-check form-switch d-flex justify-content-center">
                                                                <input class="form-check-input" type="checkbox" id="toggleDospem_{{ $dosen->id }}"
                                                                    {{ $dosen->is_dospem ? 'checked' : '' }} wire:change="toggleDospem({{ $dosen->id }})">
                                                                <label class="form-check-label" for="toggleDospem_{{ $dosen->id }}"></label>
                                                            </div>
                                                        </td>
                                                        <td class="align-middle text-center ">
                                                            <div class="form-check form-switch d-flex justify-content-center">
                                                                <input class="form-check-input" type="checkbox" id="toggleDospuj_{{ $dosen->id }}"
                                                                    {{ $dosen->is_dospuj ? 'checked' : '' }} wire:change="toggleDospuj({{ $dosen->id }})">
                                                                <label class="form-check-label" for="toggleDospuj_{{ $dosen->id }}"></label>
                                                            </div>
                                                        </td>
                                                        <td class="align-middle text-center">
                                                            <div class="form-check form-switch d-flex justify-content-center">
                                                                <input class="form-check-input" type="checkbox" id="toggleDospuj_{{ $dosen->id }}"
                                                                    {{ $dosen->is_dospuj ? 'checked' : '' }} wire:change="toggleDospuj({{ $dosen->id }})">
                                                                <label class="form-check-label" for="toggleDospuj_{{ $dosen->id }}"></label>
                                                            </div>
                                                        </td>
                                                        <td class="align-middle text-center">
                                                            <button type="button"
                                                                class="btn btn-xs btn-info mt-2">edit</button>
                                                            <button type="button"
                                                                class="btn btn-xs btn-danger mt-2">Hapus</button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        @component('components.table.table-footer', [
                                            'datas' => $dosens,
                                        ])
                                        @endcomponent
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div>
    <div class="card shadow-lg mx-4 mt-5 card-profile-bottom">
        <div class="card-body p-3">
            @component('components.table.table-header', [
                'title' => 'Data Nilai Mutu',
                'with_sort' => false,
                'is_function' => false,
                'with_filter' => true,
                'with_button' => false,
            ])
                <button wire:click="showModal('create')" class="btn bg-gradient-danger btn-sm w-100 px-3" type="button" data-bs-toggle="modal"
                    data-bs-target="#nilaimutuModal">+&nbsp;
                    <span class="d-none d-md-inline">
                        Tambah Nilai Mutu
                    </span>
                </button>
            @endcomponent
        </div>
    </div>
    <div class="container py-4">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        <div class="row gx-4">
                            <div class="card">
                                <div class="table-responsive">
                                    <table class="table align-items-center mb-0">
                                        <thead>
                                            <tr>
                                                <th
                                                    class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    NO</th>
                                                <th
                                                    class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                    Kode Mutu</th>
                                                <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    Tahun Ajaran</th>
                                                <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    Semester</th>
                                                <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    Nilai Minimal</th>
                                                <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    Nilai Maksimal</th>
                                                <th
                                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                    Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $num = ($nilaiMutus->currentPage() - 1) * $nilaiMutus->perPage() + 1;
                                            @endphp
                                            @foreach ($nilaiMutus as $item)
                                                <tr>
                                                    <td>
                                                        <div class="d-flex px-2 py-1">
                                                            <div class="d-flex flex-column justify-content-center">
                                                                <h6 class="mb-0 text-xs">{{ $num++ }}</h6>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <p class="text-xs font-weight-bold mb-0">
                                                            {{ $item->kode_mutu }}
                                                        </p>
                                                    </td>
                                                    <td class="align-middle text-center text-lg">
                                                        <span
                                                            class="text-secondary text-xs font-weight-bold">{{ $item->currentSemester->tahun_ajaran}}</span>
                                                    </td>
                                                    <td class="align-middle text-center">
                                                        <span
                                                            class="text-secondary text-xs font-weight-bold">{{ $item->currentSemester->semester }}</span>
                                                    </td>
                                                    <td class="align-middle text-center">
                                                        <span
                                                            class="text-secondary text-xs font-weight-bold">{{ $item->nilai_min }}</span>
                                                    </td>
                                                    <td class="align-middle text-center">
                                                        <span
                                                            class="text-secondary text-xs font-weight-bold">{{ $item->nilai_max }}</span>
                                                    </td>
                                                    <td class="align-middle text-center">
                                                        <a wire:click="showModal('edit', {{ $item->id }})"
                                                            class="btn btn-xs btn-info mt-2" data-bs-toggle="modal"
                                                            data-bs-target="#nilaimutuModal">
                                                            Edit
                                                        </a>
                                                        <button type="button"
                                                            class="btn btn-xs btn-danger mt-2">Hapus</button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @component('components.table.table-footer', [
                                        'datas' => $nilaiMutus,
                                    ])
                                    @endcomponent
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div wire:ignore.self class="modal nilaimutu fade" id="nilaimutuModal" tabindex="-1" role="dialog"
            aria-labelledby="nilaimutumodallabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="nilaimutumodallabel">
                            @if ($mode == 'create')
                            Tambah Nilai Mutu
                        @else
                            Edit Nilai Mutu
                        @endif
                        </h5>
                    </div>
                    <div class="modal-body">
                        <div>
                            <form>
                                <div class="form-group">
                                    <label for="kode_mutu">Kode Mutu</label>
                                    <input type="text" wire:model="kode_mutu" class="form-control">
                                    @error('kode_mutu')
                                        <small class="d-block text-danger mt-1">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="nilai_min">Nilai Minimum</label>
                                    <input type="number" wire:model="nilai_min" class="form-control">
                                    @error('nilai_min')
                                        <small class="d-block text-danger mt-1">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="nilai_max">Nilai Maksimum</label>
                                    <input type="number" wire:model="nilai_max" class="form-control">
                                    @error('nilai_max')
                                        <small class="d-block text-danger mt-1">{{ $message }}</small>
                                    @enderror
                                </div>

                                @if ($mode == 'create')
                                    <button type="submit" class="btn btn-primary" wire:click="createMutu">Simpan</button>
                                @else
                                    <button type="submit" class="btn btn-primary" wire:click="editMutu">Update</button>
                                @endif
                                <button type="button" class="btn bg-gradient-secondary"
                                    data-bs-dismiss="modal">Close</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div>
    <div class="container mt-6">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title mb-5">Input Daftar Mahasiswa</h5>
                <form wire:submit.prevent="submitForm">
                    <div class="mt-3">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <label for="nama" class="form-label">Nama</label>
                                <input type="text" class="form-control" id="nama" wire:model="nama">
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="nim" class="form-label">NIM</label>
                                <input type="text" class="form-control" id="nim" wire:model="nim">
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="wali_dosen" class="form-label">Wali Dosen</label>
                                <input type="text" class="form-control" id="wali_dosen" wire:model="wali_dosen">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="nama_prodi" class="form-label">Nama Prodi</label>
                            <input type="text" class="form-control" id="nama_prodi" wire:model="nama_prodi" disabled>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="singkatan_prodi" class="form-label">Singkatan Prodi</label>
                            <input type="text" class="form-control" id="singkatan_prodi" wire:model="singkatan_prodi"
                                disabled>
                        </div>
                    </div>
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="periode" class="form-label">Periode</label>
                    <select class="form-select" id="periode" wire:model="periode">
                        <option value="periode1">Periode 1</option>
                        <option value="periode2">Periode 2</option>
                        <option value="periode3">Periode 3</option>
                    </select>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="semester" class="form-label">Semester</label>
                    <select class="form-select" id="semester" wire:model="semester">
                        <option value="semester1">Semester 1</option>
                        <option value="semester2">Semester 2</option>
                        <option value="semester3">Semester 3</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 text-start">
                    <button type="submit" class="btn btn-primary ">Simpan</button>
                </div>
                <div class="col-md-9 text-lg-end">
                    <button class="btn btn-primary" wire:click="downloadTemplate">
                        Download template excel
                    </button>
                </div>
            </div>

            </form>

            <form wire:submit.prevent="importExcel">

                <div class="row">
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="file" class="form-label">Pilih File Excel</label>
                            <input type="file" wire:model="file" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="periode" class="form-label">Periode</label>
                        <select class="form-select" id="periode" wire:model="periode">
                            <option value="periode1">Periode 1</option>
                            <option value="periode2">Periode 2</option>
                            <option value="periode3">Periode 3</option>
                        </select>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="semester" class="form-label">Semester</label>
                        <select class="form-select" id="semester" wire:model="semester">
                            <option value="semester1">Semester 1</option>
                            <option value="semester2">Semester 2</option>
                            <option value="semester3">Semester 3</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary">Unggah Excel</button>
                </div>
            </form>

        </div>
    </div>
</div>
</div>

@extends('components.layouts.auth')

@section('content')
<main class="main-content  mt-0">
    <div class="page-header align-items-start min-vh-50 pt-5 pb-11 m-3 border-radius-lg"
        style="background-image: url('https://raw.githubusercontent.com/creativetimofficial/public-assets/master/argon-dashboard-pro/assets/img/reset-cover.jpg'); background-position: top;">
        <span class="mask bg-gradient-dark opacity-6"></span>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5 text-center mx-auto">
                    <h1 class="text-white mb-2 mt-7">Reset Password</h1>
                    <p class="text-lead text-white">You will receive an e-mail in maximum 60 seconds</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mt-lg-n10 mt-md-n11 mt-n10 justify-content-center">
            <div class="col-xl-4 col-lg-5 col-md-7 mx-auto">
                <div class="card mb-md-8">
                    <div class="card-header">
                        <div class="d-flex">
                            <div class="icon icon-shape bg-primary shadow text-center border-radius-md">
                                <i class="ni ni-circle-08 text-white text-lg opacity-10" aria-hidden="true"></i>
                            </div>
                            <div class="ms-3">
                                <h5 class="mb-0">Can't log in?</h5>
                                <p class="text-sm mb-0">Restore access to your account</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form role="form" method="POST" action="{{ route('password.email') }}">
                            @csrf
                            <label>We will send a recovery link to</label>
                            <input id="email" placeholder="Your E-mail" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary w-100 mt-4 mb-0">{{ __('Send Password Reset Link') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

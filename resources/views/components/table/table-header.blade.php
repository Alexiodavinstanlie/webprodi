<div class="mb-4 mb-md-0">
    <div class="row">
        <h5 class="col">{{ $title ?? 'Index' }}</h5>
        @if ($with_button ?? true)
        <div class="col-3">
            @if ($is_function)
            <a wire:click="{{ $click }}" class="btn bg-gradient-danger btn-sm w-100 px-3"
                type="button">+&nbsp;
                <span class="d-none d-md-inline">
                    {{ $button_title ?? __('tambah') }}
                </span>
            </a>
            @else
            <a href="{{ $click }}" class="btn bg-gradient-danger btn-sm w-100 px-3"
                type="button">+&nbsp;
                <span class="d-none d-md-inline">
                    {{ isset($button_title) ? __('tambah'). ' '.$button_title  :  __('tambah') }}
                </span>
            </a>
            @endif
        </div>
        @endif
        @if ($with_modal ?? true)
        <div class="col-3">
            <div>
                {{ $slot }}
            </div>
        </div>
        @endif
    </div>
    <div class="row">
        @if ($with_entries ?? true)
        <div class="col-md-5 col-3">
            <x-form.input-entries class="form-control-sm" :options="$entry_options ?? [5,10,15,20]"/>
        </div>
        @endif


        <div class="col-md-2 col ps-0 px-md-2 {{ $with_sort ? 'order-md-2' : 'order-md-1' }}">
            @if ($with_sort ?? false)
            <x-form.sort-by-select :options="$sort_by_options ?? ['Oldest', 'Newest']" />
            @endif
        </div>

        <div class="col-md-2 ps-0 px-md-2 col order-md-1 text-end">
            @if ($with_filter ?? false)
            <button class="btn btn-outline-secondary btn-sm w-100 rounded mb-0 px-0" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight">
                <span class="fas fa-filter me-2 fixed-plugin-button-nav"></span>
                <Span class="d-none d-md-inline">Filter</Span>
            </button>
            @endif
        </div>

        @if ($with_search ?? true)
        <div class="col-md-3 order-3">
            <x-form.search-input type="text" class="form-control-sm" placeholder="{{ $search_placehoder ?? __('Search') }}" />
        </div>
        @endif


    </div>
</div>



<div class="row mt-4">
    <div class="col mb-2 mb-md-0 text-sm mx-3">
        {{ __('menampilkan') }} {{ $datas->firstItem() }} - {{ $datas->lastItem() }} {{ __('dari') }} {{ $datas->total() }} {{ __('data') }}
    </div>
    <div class="col-md-6">
        {{ $datas->links() }}
    </div>
</div>

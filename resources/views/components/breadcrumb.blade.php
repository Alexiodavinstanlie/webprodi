<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
        @foreach ($link_items as $item => $url)

        @php
        $data = explode(';',$url);
        $urlBase = count($data) > 1 ? $data[0] : $url;
        $urlParams = $data[1] ?? '';
        $params = [];

        if ($urlParams) {
            $urlParams = explode(',', $urlParams);
            for ($i=0; $i < count($urlParams) ; $i++) {
                $exploded = explode('=>', $urlParams[$i]);
                $params[str_replace("'","",$exploded[0])] = str_replace("'","",$exploded[1]);
            }
        }
        @endphp
      <li class="breadcrumb-item text-sm text-white {{ $loop->last || $url == 'index' ? '' : 'text-white active' }}">
        <a class="{{ $loop->last || $url == 'index' ? '' : 'opacity-5' }} text-white" href="{{ $loop->last || $url == 'index' ? 'javascript:;' : route($urlBase, $params) }}">{{ $item }}
        </a>
      </li>
        @endforeach
    </ol>
    <h6 class="font-weight-bolder mb-0 text-white">{{ $page_title }}</h6>
  </nav>

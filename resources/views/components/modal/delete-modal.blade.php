<div class="modal fade gimmick_class" id="{{ $modal_id ?? "modalDelete" }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
        <div class="modal-content">
            <div class="modal-body px-5">
                <div class="text-center">
                    <div class="swal2-icon swal2-warning swal2-icon-show" style="display: flex;">
                        <div class="swal2-icon-content">!</div>
                    </div>
                    <h4 class="text-gradient text-danger mt-4">Apakah Anda Yakin Hapus Ini ?</h4>
                    <p>Data Tidak Akan Dapat Dikembalikan lagi</p>
                </div>
                <div class="text-center">
                    <button wire:click="{{ $function }}" type="button" class="swal2-confirm btn bg-gradient-success"
                        style="display: inline-block;">Hapus !</button>
                    <button type="button" class="swal2-cancel btn bg-gradient-danger" style="display: inline-block;"
                        data-bs-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</div>

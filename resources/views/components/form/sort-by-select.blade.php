<div class="form-group">
    <select wire:model="sort_by" class="form-select form-select-sm {{ $class ?? '' }}" id="sort_by">
      @foreach ($options as $option)
        <option value="{{ $option }}">{{ ucwords( $option) }}</option>
      @endforeach
    </select>
  </div>

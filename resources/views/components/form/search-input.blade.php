<div class="input-group">
    <span class="input-group-text text-body {{ $margin ?? '' }}"><i class="fas fa-search" aria-hidden="true"></i></span>
    <input wire:model.live="search" type="text" class="form-control {{ $class ?? '' }} {{ $margin ?? '' }} ps-2" placeholder="{{ $placeholder }}">
</div>

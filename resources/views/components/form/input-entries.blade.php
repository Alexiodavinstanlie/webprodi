<div class="form-group">
    <div class=" align-items-center justify-content-center">
      <select wire:model.live="entries" class="{{ $class ?? '' }} {{ $margin ?? '' }}">
        @foreach ($options as $option)
          <option value="{{ $option }}">{{ $option }}</option>
        @endforeach
      </select>
      <span class="text-secondary d-none d-md-inline m-0 text-xs {{ $span_class ?? '' }}  {{ $margin ?? '' }}">&nbsp;&nbsp; {{ ucwords(__('entries')) }}</span>
    </div>
  </div>

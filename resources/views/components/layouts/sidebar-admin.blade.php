<aside class="sidenav bg-white navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-4 "
    id="sidenav-main">
    <div class="sidenav-header text-center">
        <a class="navbar-brand m-0" href="#" target="_blank">
            <img src="{{ asset('img/logo2.png') }}" class="img-fluid dark-image" alt="main_logo">
        </a>
    </div>
    <hr class="horizontal dark mt-0">
    <div class="collapse navbar-collapse  w-auto h-auto" id="sidenav-collapse-main">
        <ul class="navbar-nav">
            <li class="nav-item">
                <ul class="nav ms-4">
                    <li class="nav-item ">
                        <a class="nav-link " href="{{ route('admin.dashboard') }}">
                            <span class="sidenav-mini-icon text-bold"> D </span>
                            <span class="sidenav-normal text-dark"> Dashboards </span>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " href="{{ route('admin.periode.index') }}">
                            <span class="sidenav-mini-icon text-bold"> P </span>
                            <span class="sidenav-normal text-dark"> Periode </span>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " href="{{ route('admin.nilaimutu.index') }}">
                            <span class="sidenav-mini-icon text-bold"> NM </span>
                            <span class="sidenav-normal text-dark"> Nilai Mutu </span>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " href="{{ route('admin.dosen.index') }}">
                            <span class="sidenav-mini-icon text-bold"> D </span>
                            <span class="sidenav-normal"> Dosen </span>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " href="{{ route('admin.mahasiswa.index') }}">
                            <span class="sidenav-mini-icon"> M </span>
                            <span class="sidenav-normal"> Mahasiswa </span>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " href="{{ route('admin.ruangan.index') }}">
                            <span class="sidenav-mini-icon"> R </span>
                            <span class="sidenav-normal"> Ruangan </span>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " href="{{ route('admin.rolepengguna.index') }}">
                            <span class="sidenav-mini-icon"> RP </span>
                            <span class="sidenav-normal"> Role Pengguna </span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item mt-3">
                <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder opacity-6">Koordinator PA</h6>
            </li>
            <li class="nav-item">
                <ul class="nav ms-4">
                    <li class="nav-item ">
                        <a class="nav-link " href="{{ route('KoordinatorPA.KKN.index') }}">
                            <span class="sidenav-mini-icon"> KKN </span>
                            <span class="sidenav-normal"> Kelola Komponen Nilai <b class="caret"></b></span>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " data-bs-toggle="collapse" aria-expanded="false" href="#usersExample">
                            <span class="sidenav-mini-icon"> UDM </span>
                            <span class="sidenav-normal"> Upload Daftar Mahasiswa <b class="caret"></b></span>
                        </a>
                        <div class="collapse " id="usersExample">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a class="nav-link " href="{{ route('upload.mahasiswa.proposal.index') }}">
                                        <span class="sidenav-mini-icon text-xs"> PRO </span>
                                        <span class="sidenav-normal"> Proposal </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="{{ route('upload.mahasiswa.prasidang.index') }}">
                                        <span class="sidenav-mini-icon text-xs"> PRA </span>
                                        <span class="sidenav-normal"> Prasidang </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="{{ route('upload.mahasiswa.sidang.index') }}">
                                        <span class="sidenav-mini-icon text-xs"> S </span>
                                        <span class="sidenav-normal"> Sidang </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " data-bs-toggle="collapse" aria-expanded="false" href="#accountExample">
                            <span class="sidenav-mini-icon"> J </span>
                            <span class="sidenav-normal"> Jadwal <b class="caret"></b></span>
                        </a>
                        <div class="collapse " id="accountExample">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a class="nav-link " href="{{ route('jadwal.prasidang.index') }}">
                                        <span class="sidenav-mini-icon text-xs"> B </span>
                                        <span class="sidenav-normal"> Jadwal Prasidang </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="{{ route('jadwal.sidang.index') }}">
                                        <span class="sidenav-mini-icon text-xs"> S </span>
                                        <span class="sidenav-normal"> Jadwal Sidang </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " data-bs-toggle="collapse" aria-expanded="false"
                            href="#projectsExample">
                            <span class="sidenav-mini-icon"> N </span>
                            <span class="sidenav-normal"> Nilai <b class="caret"></b></span>
                        </a>
                        <div class="collapse " id="projectsExample">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a class="nav-link " href="{{ route('nilai.proposal.index') }}">
                                        <span class="sidenav-mini-icon text-xs"> PRO </span>
                                        <span class="sidenav-normal"> Proposal </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href=""{{ route('nilai.prasidang.index') }}"">
                                        <span class="sidenav-mini-icon text-xs"> P </span>
                                        <span class="sidenav-normal"> Prasidang </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href=""{{ route('nilai.sidang.index') }}"">
                                        <span class="sidenav-mini-icon text-xs"> S </span>
                                        <span class="sidenav-normal"> Sidang </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " href="{{ route('progress.mahasiswa.index') }}">
                            <span class="sidenav-mini-icon"> PM </span>
                            <span class="sidenav-normal"> Progress Mahasiswa </span>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " href="{{ route('tidak.lulus.sidang.index') }}">
                            <span class="sidenav-mini-icon"> TLS </span>
                            <span class="sidenav-normal"> Tidak Lulus Sidang </span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item mt-3">
                <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder opacity-6">Dosen</h6>
            </li>

            <li class="nav-item">
                <ul>

                    <a class="nav-link " href="{{ route('dosen.dospem.index') }}">
                        <span class="sidenav-mini-icon"> PBB </span>
                        <span class="sidenav-normal"> Dosen Pembimbing </span>
                    </a>
                    <a class="nav-link " href="{{ route('dosen.dospuj.index') }}">
                        <span class="sidenav-mini-icon"> PUJ </span>
                        <span class="sidenav-normal"> Dosen Penguji </span>
                    </a>
                    <a class="nav-link " href="{{ route('dosen.doswal.index') }}">
                        <span class="sidenav-mini-icon"> Wal </span>
                        <span class="sidenav-normal"> Dosen Wali </span>
                    </a>
                </ul>

            </li>
        </ul>
    </div>
</aside>

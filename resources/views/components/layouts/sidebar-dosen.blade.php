<aside class="sidenav bg-white navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-4 " id="sidenav-main">
    <div class="sidenav-header text-center">
        <a class="navbar-brand m-0" href="#" target="_blank">
            <img src="{{ asset('img/logo2.png') }}" class="img-fluid dark-image" alt="main_logo">
        </a>
    </div>
    <hr class="horizontal dark mt-0">
    <div class="collapse navbar-collapse  w-auto h-auto" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
            <a href="{{ route('dosen.dashboard') }}" class="nav-link active" >
              <div class="icon icon-shape icon-sm text-center d-flex align-items-center justify-content-center">
                <i class="ni ni-shop text-primary text-sm opacity-10"></i>
              </div>
              <span class="nav-link-text ms-1">Dashboards</span>
            </a>
          </li>
        <li class="nav-item mt-3">
            <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder opacity-6">Dosen</h6>
          </li>
        <li class="nav-item">
          <a data-bs-toggle="collapse" href="#applicationsExamples" class="nav-link " aria-controls="applicationsExamples" role="button" aria-expanded="false">
            <div class="icon icon-shape icon-sm text-center d-flex align-items-center justify-content-center">
              <i class="ni ni-ui-04 text-info text-sm opacity-10"></i>
            </div>
            <span class="nav-link-text ms-1">Dosen</span>
          </a>
          <div class="collapse  show " id="applicationsExamples">
            <ul class="nav ms-4">
              <li class="nav-item ">
                <a class="nav-link " href="{{ route('dosen.dospem.index') }}">
                  <span class="sidenav-mini-icon"> PBB </span>
                  <span class="sidenav-normal"> Dosen Pembimbing </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="{{ route('dosen.dospuj.index') }}">
                  <span class="sidenav-mini-icon"> PUJ </span>
                  <span class="sidenav-normal"> Dosen Penguji </span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="nav-link " href="{{ route('dosen.doswal.index') }}">
                  <span class="sidenav-mini-icon"> Wal </span>
                  <span class="sidenav-normal"> Dosen Wali </span>
                </a>
              </li>
            </ul>
          </div>
        </li>
      </ul>
    </div>
  </aside>



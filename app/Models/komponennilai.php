<?php

namespace App\Models;

use App\Models\CurrentSemester;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class komponennilai extends Model
{
    use HasFactory;
    protected $table = 'komponennilais';

    protected $fillable = [
        'nama',
        'nilai_komponen',
        'nama_prodi',
    ];

    public function currentSemester(): BelongsTo
    {
        return $this->belongsTo(CurrentSemester::class);
    }
}

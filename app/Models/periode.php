<?php

namespace App\Models;

use App\Models\MahasiswaSidang;
use App\Models\PendaftaranSidang;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class periode extends Model
{
    use HasFactory;
    protected $table = 'periodes';

    const SEMESTERS_GANJIL = 'genap';
    const SEMESTERS_GENAP   = 'ganjil';

    const SEMESTERS = [
        'ganjil',
        'genap',
    ];

    const BULAN = [
        'januari',
        'februari',
        'maret',
        'april',
        'mei',
        'juni',
        'juli',
        'agustus',
        'september',
        'oktober',
        'november',
        'desember',
    ];

    protected $fillable = [
        'periode',
        'semester',
        'tahun',
        'bulan'
    ];

    public function getStatusColorAttribute()
    {
        if ($this->semester == Self::SEMESTERS_GANJIL) {
            return 'info';
        } elseif($this->semester == Self::SEMESTERS_GENAP) {
            return 'primary';
        }
    }

    public function pendaftaranSidang()
    {
        return $this->hasMany(PendaftaranSidang::class)->withTrashed();
    }

    public function mahasiswaSidang()
    {
        return $this->hasMany(MahasiswaSidang::class)->withTrashed();
    }
}

<?php

namespace App\Models;

use App\Models\User;
use App\Models\periode;
use App\Models\MahasiswaSidang;
use App\Models\PendaftaranSidang;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Mahasiswa extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama_mahasiswa',
        'nim',
        'nama_prodi',
        'singkatan_prodi',
        'periode_id'
    ];

    public function current()
    {
        return $this->belongsTo(periode::class, 'periode_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function mahasiswaSidang()
    {
        return $this->hasMany(MahasiswaSidang::class);
    }

    public function PendaftaranSidang(){
        return $this->hasMany(PendaftaranSidang::class)->withTrashed();
    }
}

<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Models\Dosen;
use App\Models\Mahasiswa;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    use HasRoles;

    const ROLE_MAHASISWA  = 0;
    const ROLE_DOSEN      = 1;
    const ROLE_ADMIN      = 2;
    const ROLE_KAPRODI    = 3;
    const ROLE_SUPERADMIN = 4;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function isAdmin()
    {
        if ($this->role == Self::ROLE_ADMIN) return true;
        return false;
    }

    public function isDosen()
    {
        if ($this->role == Self::ROLE_DOSEN) return true;
        return false;
    }

    public function isKaprodi()
    {
        if ($this->role == Self::ROLE_KAPRODI) return true;
        return false;
    }

    public function isMahasiswa()
    {
        if ($this->role == Self::ROLE_MAHASISWA) return true;
        return false;
    }

    public function isSuperAdmin()
    {
        if ($this->role == Self::ROLE_SUPERADMIN) return true;
        return false;
    }

    public function dosen()
    {
        return $this->hasOne(Dosen::class, 'user_id');
    }

    public function mahasiswa()
    {
        return $this->hasOne(Mahasiswa::class, 'user_id');
    }
}

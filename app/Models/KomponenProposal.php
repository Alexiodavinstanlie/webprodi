<?php

namespace App\Models;

use App\Models\periode;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class KomponenProposal extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'komponen_proposal';
    protected $fillable = [
        'id',
        'periode_id',
        'nama_komponen',
        'persentase',
        'keterangan',
        // 'tanggal_deadline_input_nilai',
        // 'deadline_proposal_id'
    ];

    public function periode()
    {
        return $this->belongsTo(periode::class, 'periode_id');
    }

}

<?php

namespace App\Models;

use App\Models\CurrentSemester;
use App\Models\MahasiswaSidang;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Dosen extends Model
{
    use HasFactory;
    protected $fillable = [
        'username',
        'nama_gelar',
        'nama_dosen',
        'prodi',
        'password',
        'role',
        'singkatan_dosen',
        'nip',
        'is_admin',
        'is_dospem',
        'is_dospuj',
        'is_doswal',
        'email',
        'telepon',
        'alamat',
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function currentSemester(): BelongsTo
    {
        return $this->belongsTo(CurrentSemester::class);
    }

    public function MahasiswaSidang()
    {
        return $this->hasMany(MahasiswaSidang::class)->withTrashed();
    }
}

<?php

namespace App\Models;

use App\Models\periode;
use App\Models\CurrentSemester;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Nilai_mutu extends Model
{
    use HasFactory;

    protected $fillable = ['kode_mutu', 'nilai_min', 'nilai_max','current_semester_id'];


    public function currentSemester(): BelongsTo
    {
        return $this->belongsTo(CurrentSemester::class);
    }
}

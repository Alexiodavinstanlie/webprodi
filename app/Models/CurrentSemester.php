<?php

namespace App\Models;

use App\Models\Dosen;
use App\Models\Nilai_mutu;
use App\Models\komponennilai;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CurrentSemester extends Model
{
    use HasFactory;

    protected $table = 'current_semesters';

    protected $fillable = [
        'tahun_ajaran',
        'semester',
    ];

    public function nilaiMutus()
    {
        return $this->hasMany(Nilai_mutu::class);
    }

    public function dosens()
    {
        return $this->hasMany(Dosen::class);
    }
    public function komponennilais()
    {
        return $this->hasMany(komponennilai::class);
    }
}

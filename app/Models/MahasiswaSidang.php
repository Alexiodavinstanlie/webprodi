<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MahasiswaSidang extends Model
{
    use HasFactory;
    protected $fillable = [
        'pendaftaran_sidang_id',
        'mahasiswa_id',
        'pembimbing1_id',
        'pembimbing2_id',
        'penguji1_id',
        'penguji2_id',
        'periode_id',
        'judul_indo',
        'judul_inggris',
        'tahun_ajaran',
        'semester',
        'bulan',
        'jumlah_penguji'
    ];
    public function mahasiswa()
    {
        return $this->belongsTo(Mahasiswa::class);
    }

    public function pembimbing1()
    {
        return $this->belongsTo(Dosen::class);
    }

    public function pembimbing2()
    {
        return $this->belongsTo(Dosen::class);
    }

    public function penguji1()
    {
        return $this->belongsTo(Dosen::class);
    }

    public function penguji2()
    {
        return $this->belongsTo(Dosen::class);
    }

    public function periode()
    {
        return $this->belongsTo(periode::class);
    }
}

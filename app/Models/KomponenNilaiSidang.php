<?php

namespace App\Models;

use App\Models\CurrentSemester;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class KomponenNilaiSidang extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama_sidang',
        'nilai_komponen_sidang',
    ];

    public function currentSemester(): BelongsTo
    {
        return $this->belongsTo(CurrentSemester::class);
    }
}

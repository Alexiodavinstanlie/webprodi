<?php

namespace App\Models;

use App\Models\periode;
use App\Models\Mahasiswa;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PendaftaranSidang extends Model
{
    use HasFactory;
    protected $fillable = [
        'periode_id',
        'mahasiswa_id',
        'tanggal_maksimal_daftar',
        'transkip_nilai',
        'ksm',
        'ktp',
        'ijazah',
        'surat_pernyataan',
        'status_pendaftaran',
        'tahun_ajaran',
        'semester',
    ];

    protected $casts = [
        'id' => 'integer',
        'periode_id' => 'integer',
        'mahasiswa_id' => 'integer',
    ];

    protected $with = [
        'periode',
        'mahasiswa',
    ];

    public function periode(){
        return $this->belongsTo(periode::class)->withTrashed();
    }

    public function mahasiswa(){
        return $this->belongsTo(Mahasiswa::class)->withTrashed();
    }
}

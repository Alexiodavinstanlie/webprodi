<?php

namespace App\Livewire\Pages\Auth;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class LoginPageIndex extends Component
{
    public function mount()
    {
        $user = Auth::user();
        if ($user) {
            if ($user->isAdmin()) {
                return redirect(route('admin.dashboard'));
            }
            elseif ($user->isDosen()) {
                return redirect(route('dosen.dashboard'));
            }
            elseif ($user->isKaprodi()) {
                return redirect(route('kaprodi.dashboard'));
            }
            elseif ($user->isMahasiswa()) {
                return redirect(route('mahasiswa.dashboard'));
            }
            elseif ($user->isSuperAdmin()) {
                return redirect(route('superadmin.dashboard'));
            }
        }
    }

    public function render()
    {
        return view('auth.login');
    }
}

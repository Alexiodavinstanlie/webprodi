<?php

namespace App\Livewire\Pages\Dosen\Dashboard;

use Livewire\Component;

class DosenDashboardIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.dosen.dashboard.dosen-dashboard-index');
    }
}

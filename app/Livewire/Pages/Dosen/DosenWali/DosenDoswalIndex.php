<?php

namespace App\Livewire\Pages\Dosen\DosenWali;

use Livewire\Component;

class DosenDoswalIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.dosen.dosen-wali.dosen-doswal-index');
    }
}

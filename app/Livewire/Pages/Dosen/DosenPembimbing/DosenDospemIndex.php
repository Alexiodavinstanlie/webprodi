<?php

namespace App\Livewire\Pages\Dosen\DosenPembimbing;

use Livewire\Component;

class DosenDospemIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.dosen.dosen-pembimbing.dosen-dospem-index');
    }
}

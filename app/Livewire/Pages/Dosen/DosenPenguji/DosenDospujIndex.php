<?php

namespace App\Livewire\Pages\Dosen\DosenPenguji;

use Livewire\Component;

class DosenDospujIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.dosen.dosen-penguji.dosen-dospuj-index');
    }
}

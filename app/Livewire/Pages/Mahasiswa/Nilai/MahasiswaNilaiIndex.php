<?php

namespace App\Livewire\Pages\Mahasiswa\Nilai;

use Livewire\Component;

class MahasiswaNilaiIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.mahasiswa.nilai.mahasiswa-nilai-index');
    }
}

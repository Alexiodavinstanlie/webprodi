<?php

namespace App\Livewire\Pages\Mahasiswa\Jadwal;

use Livewire\Component;

class MahasiswaJadwalIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.mahasiswa.jadwal.mahasiswa-jadwal-index');
    }
}

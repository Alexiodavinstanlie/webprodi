<?php

namespace App\Livewire\Pages\Mahasiswa\TaPa;

use Livewire\Component;

class MahasiswaTaPaIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.mahasiswa.ta-pa.mahasiswa-ta-pa-index');
    }
}

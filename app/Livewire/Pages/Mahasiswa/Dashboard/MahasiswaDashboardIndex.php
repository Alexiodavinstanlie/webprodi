<?php

namespace App\Livewire\Pages\Mahasiswa\Dashboard;

use Livewire\Component;

class MahasiswaDashboardIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.mahasiswa.dashboard.mahasiswa-dashboard-index');
    }
}

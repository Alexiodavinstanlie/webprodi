<?php

namespace App\Livewire\Pages\Kaprodi\Dashboard;

use Livewire\Component;

class KaprodiDashboardIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.kaprodi.dashboard.kaprodi-dashboard-index');
    }
}

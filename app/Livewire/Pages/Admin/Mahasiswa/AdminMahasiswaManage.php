<?php

namespace App\Livewire\Pages\Admin\Mahasiswa;

use Livewire\Component;
use App\Models\Mahasiswa;
use Livewire\WithFileUploads;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class AdminMahasiswaManage extends Component
{
    public function render()
    {
        return view('livewire.pages.admin.mahasiswa.admin-mahasiswa-manage');
    }
}

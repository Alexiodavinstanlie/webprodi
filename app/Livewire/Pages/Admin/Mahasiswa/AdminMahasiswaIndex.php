<?php

namespace App\Livewire\Pages\Admin\Mahasiswa;

use Livewire\Component;

class AdminMahasiswaIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.admin.mahasiswa.admin-mahasiswa-index');
    }
}

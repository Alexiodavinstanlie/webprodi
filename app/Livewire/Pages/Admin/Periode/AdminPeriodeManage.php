<?php

namespace App\Livewire\Pages\Admin\Periode;

use Livewire\Component;

class AdminPeriodeManage extends Component
{
    public $id_periode;
    public function mount($id_periode = null)
    {
        $this->id_periode = $id_periode;
    }

    public function render()
    {
        return view('livewire.pages.admin.periode.admin-periode-manage');
    }
}

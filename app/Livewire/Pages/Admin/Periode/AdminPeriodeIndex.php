<?php

namespace App\Livewire\Pages\Admin\Periode;

use Livewire\Component;

class AdminPeriodeIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.admin.periode.admin-periode-index');
    }
}

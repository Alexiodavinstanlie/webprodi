<?php

namespace App\Livewire\Pages\Admin\Dosen;

use Livewire\Component;

class AdminDosenManage extends Component
{
    public function render()
    {
        return view('livewire.pages.admin.dosen.admin-dosen-manage');
    }
}

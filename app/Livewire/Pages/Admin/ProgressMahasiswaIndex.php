<?php

namespace App\Livewire\Pages\Admin;

use Livewire\Component;

class ProgressMahasiswaIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.admin.progress-mahasiswa-index');
    }
}

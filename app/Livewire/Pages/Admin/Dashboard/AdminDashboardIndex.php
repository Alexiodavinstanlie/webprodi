<?php

namespace App\Livewire\Pages\Admin\Dashboard;

use Livewire\Component;

class AdminDashboardIndex extends Component
{
    
    public function render()
    {
        return view('livewire.pages.admin.dashboard.admin-dashboard-index');
    }
}

<?php

namespace App\Livewire\Pages\Admin\Rolepengguna;

use Livewire\Component;

class AdminRolepenggunaIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.admin.rolepengguna.admin-rolepengguna-index');
    }
}

<?php

namespace App\Livewire\Pages\Admin\Roledosen;

use Livewire\Component;

class AdminRoledosenIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.admin.roledosen.admin-roledosen-index');
    }
}

<?php

namespace App\Livewire\Pages\Admin\Ruangan;

use Livewire\Component;

class AdminRuanganIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.admin.ruangan.admin-ruangan-index');
    }
}

<?php

namespace App\Livewire\Pages\Admin\Kaprodi;

use Livewire\Component;

class AdminKaprodiIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.admin.kaprodi.admin-kaprodi-index');
    }
}

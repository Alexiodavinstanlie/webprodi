<?php

namespace App\Livewire\Pages\Admin\Nilaimutu;

use Livewire\Component;

class AdminNilaimutuIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.admin.nilaimutu.admin-nilaimutu-index');
    }
}

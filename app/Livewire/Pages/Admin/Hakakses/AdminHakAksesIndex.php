<?php

namespace App\Livewire\Pages\Admin\Hakakses;

use Livewire\Component;

class AdminHakAksesIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.admin.hakakses.admin-hak-akses-index');
    }
}

<?php

namespace App\Livewire\Pages\Admin\KoordinatorPA\UploadMahasiswa;

use Livewire\Component;

class Proposal extends Component
{
    public function render()
    {
        return view('livewire.pages.admin.koordinator-p-a.upload-mahasiswa.proposal-index');
    }
}

<?php

namespace App\Livewire\Pages\Admin\KoordinatorPA\UploadMahasiswa;

use Livewire\Component;

class PrasidangJadwal extends Component
{
    public $id;

    public function mount($id)
    {
        $this->id = $id;
    }


    public function render()
    {
        return view('livewire.pages.admin.koordinator-p-a.upload-mahasiswa.prasidang-jadwal');
    }
}

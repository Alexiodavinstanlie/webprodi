<?php

namespace App\Livewire\Pages\Admin\KoordinatorPA\UploadMahasiswa;

use Livewire\Component;

class PrasidangManageIndex extends Component
{
    public $id_prasidang;

    public function mount($id_prasidang = null)
    {
        $this->id_prasidang = $id_prasidang;
    }

    public function render()
    {
        return view('livewire.pages.admin.koordinator-p-a.upload-mahasiswa.prasidang-manage-index');
    }
}

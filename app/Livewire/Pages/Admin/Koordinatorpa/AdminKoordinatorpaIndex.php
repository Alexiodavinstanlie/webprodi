<?php

namespace App\Livewire\Pages\Admin\Koordinatorpa;

use Livewire\Component;

class AdminKoordinatorpaIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.admin.koordinatorpa.admin-koordinatorpa-index');
    }
}

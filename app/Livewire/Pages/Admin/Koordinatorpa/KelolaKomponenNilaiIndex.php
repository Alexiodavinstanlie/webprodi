<?php

namespace App\Livewire\Pages\Admin\KoordinatorPA;

use App\Models\Dosen;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class KelolaKomponenNilaiIndex extends Component
{
    public function render()
    {
        $userId = Auth::id();

        $dosen = Dosen::where('user_id', $userId)->first();
        return view('livewire.pages.admin.koordinator-p-a.kelola-komponen-nilai-index', compact('dosen'));
    }


}

<?php

namespace App\Livewire\Pages\Admin\Jadwal;

use Livewire\Component;

class JadwalSidangIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.admin.jadwal.jadwal-sidang-index');
    }
}

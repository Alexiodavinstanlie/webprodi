<?php

namespace App\Livewire\Pages\Admin\Jadwal;

use Livewire\Component;

class JadwalPrasidangIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.admin.jadwal.jadwal-prasidang-index');
    }
}

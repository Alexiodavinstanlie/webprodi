<?php

namespace App\Livewire\Pages\Admin;

use Livewire\Component;

class TidakLulusSidangIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.admin.tidak-lulus-sidang-index');
    }
}

<?php

namespace App\Livewire\Pages\Admin\Nilai;

use Livewire\Component;

class NilaiPrasidangIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.admin.nilai.nilai-prasidang-index');
    }
}

<?php

namespace App\Livewire\Pages\Admin\Nilai;

use Livewire\Component;

class NilaiProposalIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.admin.nilai.nilai-proposal-index');
    }
}

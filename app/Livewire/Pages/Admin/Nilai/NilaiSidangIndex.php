<?php

namespace App\Livewire\Pages\Admin\Nilai;

use Livewire\Component;

class NilaiSidangIndex extends Component
{
    public function render()
    {
        return view('livewire.pages.admin.nilai.nilai-sidang-index');
    }
}

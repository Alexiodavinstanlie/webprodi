<?php

namespace App\Livewire\Pages\Superadmin\Dashboard;

use App\Models\Dosen;
use Livewire\Component;
use Livewire\WithPagination;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class SuperadminDashboardIndex extends Component
{
    use LivewireAlert,WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $entries = 10;
    public $search;

    public $isAdmin = false;
    public $maxAdminCount = 2;
    public $adminCount = 0;


    public function render()
    {
        $dosens = Dosen::query();
        if ($this->search) {
            $dosens = $dosens->where(function($query) {
                $query->where('nama_dosen', 'like', '%' . $this->search . '%')
                      ->orWhere('nip', 'like', '%' . $this->search . '%')
                      ->orWhere('singkatan_dosen', 'like', '%' . $this->search . '%');
            });
        }
        $dosens = $dosens->orderByDesc('is_admin')
        ->orderByDesc('created_at')
        ->paginate($this->entries);

        $currentPage = $dosens->currentPage();
        $num = ($currentPage - 1) * $dosens->perPage() + 1;
        return view('livewire.pages.superadmin.dashboard.superadmin-dashboard-index',compact('dosens','currentPage','num'));
    }

    public function setAdmin($dosenId)
    {
        $dosen = Dosen::findOrFail($dosenId);
        $this->adminCount = Dosen::where('is_admin', true)->count();

        if ($dosen->is_admin) {
            $dosen->is_admin = false;
            $dosen->save();
        } else {
            if ($this->adminCount < $this->maxAdminCount) {
                $dosen->is_admin = true;
                $dosen->save();
            } else {
                $this->alert('warning', 'Jumlah admin sudah mencapai batas maksimal.');
            }
        }
    }

    public function showConfirmationModal()
    {
        $this->dispatch('showConfirmationModal');
    }

    // public function setAdmin()
    // {
    //     $dosen = Dosen::find($this->selectedDosen);
    //     if ($dosen) {
    //         $dosen->update([
    //             'is_admin' => $this->isAdmin, // Pastikan $this->isAdmin memiliki nilai yang valid
    //         ]);

    //         $this->alert('success', 'Admin Berhasil Diperbarui');
    //     } else {
    //         $this->alert('warning', 'Dosen Tidak Ditemukan');
    //     }
    // }


    // public function saveAdminChanges()
    // {
    //     foreach ($this->isAdmin as $dosenId => $isAdmin) {
    //         $dosen = Dosen::find($Id);
    //         $dosen->is_admin = $isAdmin;
    //         $dosen->save();
    //     }
    // }

}

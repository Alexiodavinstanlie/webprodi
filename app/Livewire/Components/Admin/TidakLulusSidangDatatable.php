<?php

namespace App\Livewire\Components\Admin;

use Livewire\Component;

class TidakLulusSidangDatatable extends Component
{
    public function render()
    {
        return view('livewire.components.admin.tidak-lulus-sidang-datatable');
    }
}

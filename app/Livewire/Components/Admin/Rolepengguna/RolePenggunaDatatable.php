<?php

namespace App\Livewire\Components\Admin\Rolepengguna;

use App\Models\Dosen;
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\CurrentSemester;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class RolePenggunaDatatable extends Component
{
    use LivewireAlert, WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $entries = 10;
    public $search;

    public $isKaprodi = false;
    public $maxKaprodiCount = 1;
    public $kaprodiCount = 0;
    public $currentSemester;

    public function mount()
    {
        $this->getCurrentSemester();
    }

    public function getCurrentSemester()
    {
        $this->currentSemester = CurrentSemester::latest()->first();
    }

    public function render()
    {
        $dosens = Dosen::query();
        if ($this->search) {
            $dosens = $dosens->where(function($query) {
                $query->where('nama_dosen', 'like', '%' . $this->search . '%')
                      ->orWhere('nip', 'like', '%' . $this->search . '%')
                      ->orWhere('singkatan_dosen', 'like', '%' . $this->search . '%');
            });
        }
        $dosens = $dosens->orderByDesc('is_kaprodi')
        ->orderByDesc('created_at')
        ->paginate($this->entries);

        $currentPage = $dosens->currentPage();
        $num = ($currentPage - 1) * $dosens->perPage() + 1;
        return view('livewire..components.admin.rolepengguna.role-pengguna-datatable',compact('dosens','currentPage','num'));
    }

    public function setKaprodi($dosenId)
    {
        $dosen = Dosen::findOrFail($dosenId);
        $this->kaprodiCount = Dosen::where('is_kaprodi', true)->count();

        if ($dosen->is_kaprodi) {
            $dosen->is_kaprodi = false;
            $dosen->save();
        } else {
            if ($this->kaprodiCount < $this->maxKaprodiCount) {
                $dosen->is_kaprodi = true;
                $dosen->save();
            } else {
                $this->alert('warning', 'Kaprodi sudah mencapai batas maksimal.');
            }
        }
    }
}


<?php

namespace App\Livewire\Components\Admin\Periode;

use App\Models\periode;
use Livewire\Component;
use Illuminate\Support\Facades\Config;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class PeriodeManage extends Component
{
    use LivewireAlert;
    public $periode;
    public $tahun;
    public $semester;
    public $bulan;
    public $isEditing = '';
    public $periode_data;

    public function mount($id_periode = null)
    {
        if ($id_periode) {
            $this->periode_data = periode::findOrFail($id_periode);
            $this->periode      = $this->periode_data->periode;
            $this->tahun        = $this->periode_data->tahun;
            $this->semester     = $this->periode_data->semester;
            $this->bulan        = $this->periode_data->bulan;
        }
    }


    public function render()
    {
        $months = periode::BULAN;
        return view('livewire.components.admin.periode.periode-manage', compact('months'));
    }

    public function updatePeriode()
    {
        $this->periode_data->update([
            'periode' => $this->periode,
            'tahun' => $this->tahun,
            'semester' => $this->semester,
            'bulan' => $this->bulan,
        ]);

        $this->resetInputFields();
        $this->flash('success', 'Data Periode berhasil diperbarui', [], route('admin.periode.index'));

    }

    public function createPeriode()
    {
        $this->validate([
            'periode' => 'required',
            'semester' => 'required',
            'tahun' => 'required',
            'bulan' => 'required'
        ]);

        periode::create([
            'periode' => $this->periode,
            'tahun' => $this->tahun,
            'semester' => $this->semester,
            'bulan' => $this->bulan,
        ]);

        $this->resetInputFields();
        $this->flash('success', 'Data Periode berhasil disimpan', [], route('admin.periode.index'));

    }

    private function resetInputFields()
    {
        $this->periode = '';
        $this->tahun = '';
        $this->semester = '';
        $this->bulan = '';
        $this->isEditing = false;
    }
}

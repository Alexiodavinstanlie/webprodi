<?php

namespace App\Livewire\Components\Admin\Periode;

use App\Models\periode;
use Livewire\Component;
use Livewire\WithPagination;
use Jantinnerezo\LivewireAlert\LivewireAlert;


class PeriodeDatatable extends Component
{
    use WithPagination, LivewireAlert;
    public $entries = 10;
    public $options = [5, 10, 15, 20];
    protected $paginationTheme = 'bootstrap';

    public $semester;
    public $semesters = [];
    public $data = [];
    public $search;
    public $periode_data;
    public $semesterFilter;
    public $semestersvalue = [];


    public function render()
    {
        $periodes = periode::query();


        if ($this->semesterFilter) {
            $periodes = $periodes->where('semester', $this->semesterFilter);
        }

        if ($this->search) {
            $periodes = $periodes->where(function($query) {
                $query->where('periode', 'like', '%' . $this->search . '%')
                      ->orWhere('semester', 'like', '%' . $this->search . '%')
                      ->orWhere('bulan', 'like', '%' . $this->search . '%');
            });
        }

        $periodes = $periodes->orderBy('created_at','DESC')->paginate($this->entries);
        $this->semestersvalue = periode::SEMESTERS;
        $currentPage = $periodes->currentPage();
        $num = ($currentPage - 1) * $periodes->perPage() + 1;
        return view('livewire.components.admin.periode.periode-datatable',compact('periodes','currentPage','num'));
    }

    public function applyFilter()
    {
        $this->render();
    }

    public function selectCommand($type, $id){
        if ($type == 'delete') {
            $this->selected_data_id = $id;
            $this->dispatch('openModalDelete');
        }
    }

    public function delete() {
        $data = periode::findOrFail($this->selected_data_id);
        $data->delete();
        $this->alert('success', 'Data Berhasil didelete', [
            'position' => 'top-end'
        ]);
        $this->dispatch('closeModalDelete');
        $this->alert('success', 'Data berhasil dihapus');
    }

}

<?php

namespace App\Livewire\Components\Admin\Mahasiswa;

use Livewire\Component;
use App\Models\Mahasiswa;
use Livewire\WithFileUploads;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Maatwebsite\Excel\Facades\Excel;

class ComponentMahasiswaManage extends Component
{
    use LivewireAlert, WithFileUploads;
    public $nama_prodi;
    public $singkatan_prodi;
    public $nama;
    public $nim;
    public $wali_dosen;
    public $periode;
    public $semester;
    public $file;

    public function __construct()
    {
        $this->nama_prodi = "D3 SISTEM INFORMASI";
        $this->singkatan_prodi = "D3 SI";
    }

    public function submitForm()
    {
        $this->validate([
            'nama'       => 'required',
            'nim'        => 'required|unique:mahasiswa,nim',
            'wali_dosen' => 'required',
            'periode'    => 'required',
            'semester'   => 'required',
        ]);

        Mahasiswa::create([
            'nama_prodi'      => $this->nama_prodi,
            'singkatan_prodi' => $this->singkatan_prodi,
            'nama'            => $this->nama,
            'nim'             => $this->nim,
            'wali_dosen'      => $this->wali_dosen,
            'periode'         => $this->periode,
            'semester'        => $this->semester,
        ]);

        $this->resetForm();

        $this->alert('success', 'Data Mahasiswa berhasil disimpan');

        return redirect()->route('admin.mahasiswa.index');
    }

    public function resetForm()
    {
        $this->nama = '';
        $this->nim = '';
        $this->wali_dosen = '';
        $this->periode = '';
        $this->semester = '';
    }

    public function importExcel()
    {
        $this->validate([
            'file' => 'required|mimes:xls,xlsx',
        ]);

        Excel::import(new MahasiswaImport, $this->file);

        $this->resetExcel();
    }

    public function resetExcel()
    {
        $this->file = null;
    }

    public function downloadTemplate()
    {
        // Implementasi download template di sini jika diperlukan
    }


    public function render()
    {
        return view('livewire.components.admin.mahasiswa.component-mahasiswa-manage');
    }
}

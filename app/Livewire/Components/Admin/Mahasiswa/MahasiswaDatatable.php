<?php

namespace App\Livewire\Components\Admin\Mahasiswa;

use Livewire\Component;
use App\Models\Mahasiswa;
use Livewire\WithPagination;

class MahasiswaDatatable extends Component
{
    use WithPagination;
    public $search = '';
    public $entries = 10;
    public $options = [5, 10, 15, 20];
    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        $mahasiswas = Mahasiswa::query();

        if ($this->search) {
            $mahasiswas = $mahasiswas->where(function($query) {
                $query->where('nama_mahasiswa', 'like', '%' . $this->search . '%')
                      ->orWhere('nim', 'like', '%' . $this->search . '%')
                      ->orWhere('tahun_masuk', 'like', '%' . $this->search . '%');
            });
        }

        $mahasiswas = $mahasiswas->orderBy('created_at','DESC')->paginate($this->entries);

        $currentPage = $mahasiswas->currentPage();
        $num = ($currentPage - 1) * $mahasiswas->perPage() + 1;
        return view('livewire.components.admin.mahasiswa.mahasiswa-datatable',compact('mahasiswas','currentPage','num'));
    }
}

<?php

namespace App\Livewire\Components\Admin\Nilaimutu;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\CurrentSemester;
use App\Models\Nilai_mutu;
use Illuminate\Support\Facades\Validator;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class NilaimutuDatatable extends Component
{
    use LivewireAlert, WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $entries = 10;
    public $kode_mutu;
    public $mutuId;
    public $nilai_min;
    public $nilai_max;
    public $mode = 'create';
    public $currentSemester;

    protected $rules = [
        'kode_mutu' => 'required|unique:nilai_mutus',
        'nilai_min' => 'required|numeric|min:0',
        'nilai_max' => 'required|numeric|max:100',
    ];

    public function mount()
    {
        $this->getCurrentSemester();
    }

    public function getCurrentSemester()
    {
        $this->currentSemester = CurrentSemester::latest()->first();
    }

    public function render()
    {

        $nilaiMutus = Nilai_mutu::query();
        $nilaiMutus = $nilaiMutus->orderBy('created_at','DESC')->paginate($this->entries);
        $currentPage = $nilaiMutus->currentPage();
        return view('livewire.components.admin.nilaimutu.nilaimutu-datatable',compact('currentPage','nilaiMutus'));
    }

    public function createMutu()
    {
        $this->validate();

        $existingData = Nilai_mutu::where('kode_mutu', $this->kode_mutu)->first();
        if ($existingData) {
        $this->dispatch('closeModalNilaimutu');
        $this->alert('warning', 'Data dengan kode mutu ini sudah ada di database.');
        }

        Nilai_mutu::create([
            'kode_mutu' => $this->kode_mutu,
            'nilai_min' => $this->nilai_min,
            'nilai_max' => $this->nilai_max,
        ]);

        $this->reset(['kode_mutu', 'nilai_min', 'nilai_max']);
        $this->dispatch('closeModalNilaimutu');
        $this->alert('success', 'Data Nilai berhasil disimpan');
    }
    public function editMutu()
    {
        $this->validate();

        $mutu = Nilai_mutu::findOrFail($this->mutuId);

        if ($mutu) {
            $mutu->kode_mutu = $this->kode_mutu;
            $mutu->nilai_min = $this->nilai_min;
            $mutu->nilai_max = $this->nilai_max;
            $mutu->save();
        }

        $this->reset(['kode_mutu', 'nilai_min', 'nilai_max']);
        $this->dispatch('closeModalNilaimutu');
        $this->alert('success', 'Data Nilai berhasil diperbarui');
    }

    public function showModal($mode, $id_mutu = null)
    {
        $this->mode = $mode;

        if ($id_mutu) {
            $this->mutuId = Nilai_mutu::findOrFail($id_mutu);
            $this->kode_mutu = $this->mutuId->kode_mutu;
            $this->nilai_min = $this->mutuId->nilai_min;
            $this->nilai_max = $this->mutuId->nilai_max;

        }

        $this->dispatch('openModalNilaimutu');
    }


}

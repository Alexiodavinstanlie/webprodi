<?php

namespace App\Livewire\Components\Admin\Nilai;

use Livewire\Component;

class NilaiSidangDatatable extends Component
{
    public function render()
    {
        return view('livewire.components.admin.nilai.nilai-sidang-datatable');
    }
}

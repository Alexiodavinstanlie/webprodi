<?php

namespace App\Livewire\Components\Admin\KoordinatorPA\UploadMahasiswa;

use App\Models\Dosen;
use App\Models\periode;

use Livewire\Component;
use App\Models\Mahasiswa;
use App\Models\TahunAjaran;
use Illuminate\Http\Request;
use Livewire\WithFileUploads;
use App\Models\CurrentSemester;
use App\Models\MahasiswaSidang;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Facades\Response;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Protection;

class UploadPrasidangManage extends Component
{
    use WithFileUploads,LivewireAlert;
    public $mahasiswa_id;
    public $pembimbing1_id;
    public $pembimbing2_id;
    public $penguji1_id;
    public $penguji2_id;
    public $periode_id;
    public $judul_indo;
    public $judul_inggris;
    public $tahun_ajaran;
    public $semester;
    public $prasidang_data;
    public $iteration_file = 1;
    public $file;
    public $search;
    public $periodes;
    public $list_tahun_ajaran;
    public $mahasiswas;
    public $dosens;

    public function mount($id_prasidang = null)
    {

        if ($id_prasidang) {
            $this->prasidang_data = MahasiswaSidang::findOrFail($id_prasidang);
            $this->mahasiswa_id   = $this->prasidang_data->mahasiswa_id;
            $this->pembimbing1_id = $this->prasidang_data->pembimbing1_id;
            $this->pembimbing2_id = $this->prasidang_data->pembimbing2_id;
            $this->penguji1_id    = $this->prasidang_data->penguji1_id;
            $this->penguji2_id    = $this->prasidang_data->penguji2_id;
            $this->judul_indo     = $this->prasidang_data->judul_indo;
            $this->judul_inggris  = $this->prasidang_data->judul_inggris;
            $this->tahun_ajaran   = $this->prasidang_data->tahun_ajaran;
            $this->semester       = $this->prasidang_data->semester;
            $this->bulan          = $this->prasidang_data->bulan;
        }

        $this->periodes = periode::orderBy('periode', 'ASC')->get();
        $this->list_tahun_ajaran = TahunAjaran::where('is_active', 1)->get();
        $this->mahasiswas = Mahasiswa::orderBy('nama_mahasiswa', 'ASC')->get();
        $this->dosens = Dosen::orderBy('nama_dosen', 'ASC')->get();
    }
    public function render()
    {

        $months = periode::BULAN;
        $currentSemester = CurrentSemester::first();
        if ($currentSemester) {
            $this->tahun_ajaran = $currentSemester->tahun_ajaran;
        }
        return view('livewire.components.admin.koordinator-p-a.upload-mahasiswa.upload-prasidang-manage',[
        ] ,compact('months','currentSemester'));
    }

    public function updatedSemester($value)
    {
        if ($value == 'Genap') {
            $this->bulan = '';
        } elseif ($value == 'Ganjil') {
            $this->bulan = ''; // Reset nilai bulan saat memilih "Ganjil"
        }
    }

    public function createData()
    {
        $this->validate([
            'mahasiswa_id' => 'required',
            'pembimbing1_id' => 'required',
            'pembimbing2_id' => 'required',
            'penguji1_id' => 'required',
            'penguji2_id' => 'required',
            'judul_indo' => 'required|string',
            'judul_inggris' => 'required|string',
            'tahun_ajaran' => 'required',
            'semester' => 'required',
            'bulan' => 'required',
        ]);
        $currentSemester = CurrentSemester::first();

        if ($currentSemester) {
            // Ambil nilai tahun_ajaran dari model CurrentSemester
            $tahun_ajaran = $currentSemester->tahun_ajaran;

            MahasiswaSidang::create([
                'mahasiswa_id' => $this->mahasiswa_id,
                'pembimbing1_id' => $this->pembimbing1_id,
                'pembimbing2_id' => $this->pembimbing2_id,
                'penguji1_id' => $this->penguji1_id,
                'penguji2_id' => $this->penguji2_id,
                'judul_indo' => $this->judul_indo,
                'judul_inggris' => $this->judul_inggris,
                'tahun_ajaran' => $this->tahun_ajaran,
                'semester' => $this->semester,
                'bulan' => $this->bulan,
            ]);


    }

        $this->resetInputFields();
        $this->flash('success', 'Data Mahasiswa berhasil disimpan', [], route('upload.mahasiswa.prasidang.index'));

    }


    public function updateData()
    {
        Periode::find($this->periode_data->id)->update([
            'tahun_ajaran' => $this->tahun_ajaran,
            'semester' => $this->semester,
            'bulan' => $this->bulan,
            'mahasiswa_id' => $this->mahasiswa_id,
            'judul_pa' => $this->judul_pa,
            'pembimbing_1' => $this->pembimbing_1,
            'pembimbing_2' => $this->pembimbing_2,
            'penguji_1' => $this->penguji_1,
            'penguji_2' => $this->penguji_2,
        ]);

        $this->resetInputFields();

    }

    public function editData($id)
    {
        $this->data_mahasiswa = 'Edit';
        $this->data_mahasiswa = MahasiswaSidang::find($id); // Ambil data mahasiswa berdasarkan ID yang diklik
    }


    private function resetInputFields()
    {
        $this->mahasiswa_id = null;
        $this->pembimbing1_id = null;
        $this->pembimbing2_id = null;
        $this->penguji1_id = null;
        $this->penguji2_id = null;
        $this->judul_indo = null;
        $this->judul_inggris = null;
        $this->tahun_ajaran = null;
        $this->semester = null;
        $this->bulan = null;
    }


    public function exportTemplate()
    {
        $spreadsheet = new Spreadsheet();
        $worksheet   = $spreadsheet->getActiveSheet();

        $worksheet->mergeCells('A1:I2');

        $styleHeader = [
            'font' => [
                'bold'  => true,
                'color' => ['rgb' => '000000'],
                'size'  => 14,
                'name'  => 'Arial'
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical'   => Alignment::VERTICAL_CENTER,
            ],
            'fill' => [
                'fillType'   => Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => 'FFFF00',
                ],
            ],
        ];
        $styleHeader2 = [
            'font' => [
                'bold' => true,
                'color' => ['rgb' => '#000000'],
                'size' => 12,
                'name' => 'Arial'
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ],
        ];
        $styleIsiKolom = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ],
        ];

        $worksheet->getStyle('A1:I3')->applyFromArray($styleHeader);
        $worksheet->getStyle('A3:I3')->applyFromArray($styleHeader2);
        $worksheet->getStyle('A1:I503')->applyFromArray($styleIsiKolom);

        $worksheet->setCellValue('A1', 'Upload Daftar Mahasiswa');
        $worksheet->setCellValue('A3', 'NIM');
        $worksheet->setCellValue('B3', 'Nama');
        $worksheet->setCellValue('C3', 'Judul Bahasa Indonesia');
        $worksheet->setCellValue('D3', 'Judul Bahasa Inggris');
        $worksheet->setCellValue('E3', 'ID Pembimbing 1');
        $worksheet->setCellValue('F3', 'ID Pembimbing 2');
        $worksheet->setCellValue('G3', 'ID Penguji 1');
        $worksheet->setCellValue('H3', 'ID Penguji 2');
        $worksheet->setCellValue('I3', 'ID Periode');


        $worksheet->getColumnDimension('A')->setWidth(35);
        $worksheet->getColumnDimension('B')->setWidth(35);
        $worksheet->getColumnDimension('C')->setWidth(35);
        $worksheet->getColumnDimension('D')->setWidth(35);
        $worksheet->getColumnDimension('E')->setWidth(35);
        $worksheet->getColumnDimension('F')->setWidth(35);
        $worksheet->getColumnDimension('G')->setWidth(35);
        $worksheet->getColumnDimension('H')->setWidth(35);
        $worksheet->getColumnDimension('I')->setWidth(35);
        $worksheet->getProtection()->setSheet(true);

        $worksheet->getStyle('A4:I503')->getProtection()->setLocked(Protection::PROTECTION_UNPROTECTED);

        $writer = new Xlsx($spreadsheet);

        $headers = [
            'Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Content-Disposition' => 'attachment; filename="'.'Input Data Mahasiswa Prasidang'.'.xlsx"',
        ];

        $response = Response::stream(
            function () use ($writer) {
                $writer->save('php://output');
            },
            200,
            $headers
        );

        return $response;
    }


    public function createDataUpload()
    {
        $this->validate([
            'file' => 'required|mimes:xlsx'
        ]);

        $reader      = IOFactory::createReaderForFile($this->file->getRealPath());
        $spreadsheet = $reader->load($this->file->getRealPath());
        $worksheet   = $spreadsheet->getActiveSheet();
        $rowCount    = $worksheet->getHighestDataRow();

        for ($row = 4; $row <= $rowCount + 1; $row++) {
            $nim       = $worksheet->getCell('A' . $row)->getValue();
            $nama_mahasiswa          = $worksheet->getCell('B' . $row)->getValue();
            $password       = $worksheet->getCell('C' . $row)->getValue();
            $nama_dosen     = $worksheet->getCell('D' . $row)->getValue();
            $nama_gelar     = $worksheet->getCell('E' . $row)->getValue();
            $nip            = $worksheet->getCell('F' . $row)->getValue();
            $singkatan_dosen= $worksheet->getCell('G' . $row)->getValue();
            $alamat         = $worksheet->getCell('H' . $row)->getValue();

            $cek = Mahasiswa::where('email', $email)->count();

            if(empty($nama_dosen)){
                $this->clearFile();
                $this->flash('warning', 'Upload failed. Name is empty.');
            } else {
                if ($cek > 0) {
                    $this->clearFile();
                    $this->flash('warning', 'Duplicate data found.');
                } else {
                    $newDosen = Dosen::create([
                        'username' => $username,
                        'nama_gelar' => $nama_gelar,
                        'nama_dosen' => $nama_dosen,
                        'prodi' => $this->prodi,
                        'password' => Hash::make($password),
                        'role' => 'dosen',
                        'singkatan_dosen' => $singkatan_dosen,
                        'nip' => $nip,
                        'email' => $email,
                        'telepon' => $telepon,
                        'alamat' => $alamat,
                    ]);

                    if ($newDosen) {
                        $this->flash('success', 'Data berhasil disimpan.');
                        return redirect()->route('admin.dosen.index');
                    } else {
                        $this->flash('error', 'Gagal menyimpan data.');
                        return redirect()->route('admin.dosen.index');
                    }
                }
            }
        }
        $this->clearFile();
    }

    public function uploadDataMahasiswaSidang(Request $request)
    {
        // Validasi request
        $this->validate($request, [
            'file' => 'required|mimes:xlsx'
        ]);

        // Membaca file yang diunggah
        $reader = IOFactory::createReaderForFile($request->file('file')->getPathname());
        $spreadsheet = $reader->load($request->file('file')->getPathname());
        $worksheet = $spreadsheet->getActiveSheet();
        $rowCount = $worksheet->getHighestDataRow();

        // Iterasi melalui baris dalam file Excel
        for ($row = 4; $row <= $rowCount; $row++) {
            // Mengambil data dari setiap kolom
            $data = [
                'nim' => $worksheet->getCell('A' . $row)->getValue(),
                'nama_mahasiswa' => $worksheet->getCell('B' . $row)->getValue(),
                'judul_indo' => $worksheet->getCell('C' . $row)->getValue(),
                'judul_inggris' => $worksheet->getCell('D' . $row)->getValue(),
                'pembimbing1_id' => $worksheet->getCell('E' . $row)->getValue(),
                'pembimbing2_id' => $worksheet->getCell('F' . $row)->getValue(),
                'penguji1_id' => $worksheet->getCell('G' . $row)->getValue(),
                'penguji2_id' => $worksheet->getCell('H' . $row)->getValue(),
                // Sesuaikan kolom dengan field pada database
            ];

            // $cek = MahasiswaSidang::where('nim', $nim)->count();

            // if(empty($nama_dosen)){
            //     $this->clearFile();
            //     $this->flash('warning', 'Upload failed. Name is empty.');
            // } else {
            //     if ($cek > 0) {
            //         $this->clearFile();
            //         $this->flash('warning', 'Duplicate data found.');
            //     } else {
            //         $newDosen = Dosen::create([
            //             'username' => $username,
            //             'nama_gelar' => $nama_gelar,
            //             'nama_dosen' => $nama_dosen,
            //             'prodi' => $this->prodi,
            //             'password' => Hash::make($password),
            //             'role' => 'dosen',
            //             'singkatan_dosen' => $singkatan_dosen,
            //             'nip' => $nip,
            //             'email' => $email,
            //             'telepon' => $telepon,
            //             'alamat' => $alamat,
            //         ]);

            //         if ($newDosen) {
            //             $this->flash('success', 'Data berhasil disimpan.');
            //             return redirect()->route('admin.dosen.index');
            //         } else {
            //             $this->flash('error', 'Gagal menyimpan data.');
            //             return redirect()->route('admin.dosen.index');
            //         }
            //     }
            // }
            try {
                DB::beginTransaction();
                $data = [
                    'nim' => '1234567890',
                    'nama_mahasiswa' => 'John Doe',
                    'judul_indo' => 'Judul dalam Bahasa Indonesia',
                    'judul_inggris' => 'Title in English',
                    'pembimbing1_id' => 1,
                    'pembimbing2_id' => 2,
                    'penguji1_id' => 3,
                    'penguji2_id' => 4,
                    // Sesuaikan kolom dengan data yang ingin dimasukkan
                ];

                // Memasukkan data ke dalam tabel menggunakan metode create dari model MahasiswaSidang
                MahasiswaSidang::create($data);

                // Menyimpan data ke dalam database
                $mahasiswaSidang = MahasiswaSidang::create($data);

                // Tindakan lainnya jika diperlukan
                // ...

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                // Menghandle error jika gagal menyimpan data
                return back()->with('error', 'Failed to upload. Please try again.');
            }
        }

        // Jika proses upload sukses
        return back()->with('success', 'Data has been uploaded successfully.');
    }


    public function clearFile(){
        $this->file = null;
        $this->iteration_file++;
    }
}

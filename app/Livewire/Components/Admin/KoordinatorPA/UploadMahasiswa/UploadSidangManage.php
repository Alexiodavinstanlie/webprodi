<?php

namespace App\Livewire\Components\Admin\KoordinatorPA\UploadMahasiswa;

use Livewire\Component;

class UploadSidangManage extends Component
{
    public function render()
    {
        return view('livewire.components.admin.koordinator-p-a.upload-mahasiswa.upload-sidang-manage');
    }
}

<?php

namespace App\Livewire\Components\Admin\KoordinatorPA\UploadMahasiswa;

use Livewire\Component;
use App\Models\MahasiswaSidang;

class PrasidangJadwal extends Component
{
    public $mahasiswaSidang;

    public function mount($id)
    {
        $this->mahasiswaSidang = MahasiswaSidang::findOrFail($id);
    }

    public function render()
    {
        return view('livewire.components.admin.koordinator-p-a.upload-mahasiswa.prasidang-jadwal');
    }
}

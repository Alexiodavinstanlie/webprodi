<?php

namespace App\Livewire\Components\Admin\KoordinatorPA\UploadMahasiswa;

use Livewire\Component;

class UploadProposalManage extends Component
{
    public function render()
    {
        return view('livewire.components.admin.koordinator-p-a.upload-mahasiswa.upload-proposal-manage');
    }
}

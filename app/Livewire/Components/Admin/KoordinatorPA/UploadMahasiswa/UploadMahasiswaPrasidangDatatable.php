<?php

namespace App\Livewire\Components\Admin\KoordinatorPA\UploadMahasiswa;

use Livewire\Component;
use App\Models\MahasiswaSidang;

class UploadMahasiswaPrasidangDatatable extends Component
{
    public function render()
    {

        $datas = MahasiswaSidang::all();
        return view('livewire.components.admin.koordinator-p-a.upload-mahasiswa.upload-mahasiswa-prasidang-datatable',compact('datas'));
    }
}

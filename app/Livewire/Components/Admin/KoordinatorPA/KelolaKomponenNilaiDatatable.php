<?php

namespace App\Livewire\Components\Admin\KoordinatorPA;

use Livewire\Component;
use App\Models\komponennilai;
use App\Models\CurrentSemester;
use App\Models\KomponenNilaiSidang;
use App\Models\KomponenNilaiPrasidang;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class KelolaKomponenNilaiDatatable extends Component
{

    use LivewireAlert;
    public $nama;
    public $nilai_komponen;
    public $nama_prasidang;
    public $nilai_komponen_prasidang;
    public $nama_sidang;
    public $nilai_komponen_sidang;
    public $nomorKN = 1;
    public $nomorKNPrasidang = 1;
    public $nomorKNSidang = 1;
    public $currentSemester;
    public $isEditing = false;
    public $mode = 'create';

    public function render()
    {
        $this->currentSemester = CurrentSemester::latest()->first();
        $komponen_nilais = komponennilai::orderBy('created_at', 'DESC')->get();
        $komponen_nilai_prasidangs = KomponenNilaiPrasidang::orderBy('created_at', 'DESC')->get();
        $komponen_nilai_sidangs = KomponenNilaiSidang::orderBy('created_at', 'DESC')->get();
        $this->nomorKN = 1;
        return view('livewire.components.admin.koordinator-p-a.kelola-komponen-nilai-datatable',compact ('komponen_nilais','komponen_nilai_prasidangs','komponen_nilai_sidangs'));
    }

    public function createComponent()
    {
        $this->validate([
            'nama' => 'required',
            'nilai_komponen' => 'required',
        ]);

        $existingData = komponennilai::where('nama', $this->nama)->first();
        if ($existingData) {
        $this->dispatch('closeModalNilai');
        $this->alert('warning', 'Data dengan nama ini sudah ada di database.');
        }

        komponennilai::create([
            'nama' => $this->nama,
            'nilai_komponen' => $this->nilai_komponen,
        ]);


        // Setelah melakukan operasi yang diinginkan, reset input dan tambahkan nomor KN
        $this->resetInput();
        $this->nomorKN++;
        $this->isEditing = false;
        $this->dispatch('closeModalNilai');
        $this->alert('success', 'Data Nilai berhasil diperbarui');

    }

    public function editComponent()
    {
        $this->validate([
            'nama_' => 'required',
            'nilai_komponen' => 'required',
        ]);

        $komponen = komponennilai::findOrFail($this->komponenId);

        if ($komponen) {
            $komponen->nama = $this->nama;
            $komponen->nilai_komponen = $this->nilai_komponen;
            $komponen->save();
        }

        $this->resetInput();
        $this->dispatch('closeModalNilai');
        $this->alert('success', 'Data Nilai berhasil diperbarui');
    }

    public function showModal($mode, $id_komponen = null)
    {
        $this->mode = $mode;

        if ($id_komponen) {
            $this->komponenId = komponennilai::findOrFail($id_komponen);
            $this->nama = $this->komponenId->nama;
            $this->nilai_komponen = $this->komponenId->nilai_komponen;
        }

        $this->dispatch('openModalNilai');
    }


    public function createComponentPrasidang()
    {
        $this->validate([
            'nama_prasidang' => 'required',
            'nilai_komponen_prasidang' => 'required',
        ]);

        KomponenNilaiPrasidang::create([
            'nama_prasidang' => $this->nama_prasidang,
            'nilai_komponen_prasidang' => $this->nilai_komponen_prasidang,
        ]);

        // Setelah melakukan operasi yang diinginkan, reset input dan tambahkan nomor KN
        $this->resetInput();
        $this->nomorKN++;
        $this->dispatch('closeModalNilaiPrasidang');
        $this->alert('success', 'Data Nilai berhasil diperbarui');

    }


    public function createComponentSidang()
    {
        $this->validate([
            'nama_sidang' => 'required',
            'nilai_komponen_sidang' => 'required',
        ]);

        KomponenNilaiSidang::create([
            'nama_sidang' => $this->nama_sidang,
            'nilai_komponen_sidang' => $this->nilai_komponen_sidang,
        ]);

        // Setelah melakukan operasi yang diinginkan, reset input dan tambahkan nomor KN
        $this->resetInput();
        $this->nomorKN++;
        $this->dispatch('closeModalNilaiSidang');
        $this->alert('success', 'Data Nilai berhasil diperbarui');

    }



    private function resetInput()
    {
        $this->nama = '';
        $this->nilai_komponen = '';
        $this->nama_sidang = '';
        $this->nilai_komponen_sidang = '';
        $this->nama_prasidang = '';
        $this->nilai_komponen_prasidang = '';
    }

}

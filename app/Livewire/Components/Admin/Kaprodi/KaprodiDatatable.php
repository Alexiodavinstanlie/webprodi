<?php

namespace App\Livewire\Components\Admin\Kaprodi;

use Livewire\Component;

class KaprodiDatatable extends Component
{
    public function render()
    {
        return view('livewire.components.admin.kaprodi.kaprodi-datatable');
    }
}

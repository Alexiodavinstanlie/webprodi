<?php

namespace App\Livewire\Components\Admin\Jadwal;

use Livewire\Component;

class JadwalSidangDatatable extends Component
{
    public function render()
    {
        return view('livewire.components.admin.jadwal.jadwal-sidang-datatable');
    }
}

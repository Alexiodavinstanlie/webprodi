<?php

namespace App\Livewire\Components\Admin\Jadwal;

use Livewire\Component;

class JadwalPrasidangDatatable extends Component
{
    public function render()
    {
        return view('livewire.components.admin.jadwal.jadwal-prasidang-datatable');
    }
}

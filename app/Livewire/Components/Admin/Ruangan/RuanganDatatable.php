<?php

namespace App\Livewire\Components\Admin\Ruangan;

use App\Models\Ruangan;
use Livewire\Component;
use Livewire\WithPagination;

class RuanganDatatable extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $entries = 10;
    public function render()
    {
        $ruangans = Ruangan::query();
        $ruangans = $ruangans->orderBy('created_at','DESC')->paginate($this->entries);
        $currentPage = $ruangans->currentPage();
        return view('livewire.components.admin.ruangan.ruangan-datatable',compact('ruangans','currentPage'));
    }
}

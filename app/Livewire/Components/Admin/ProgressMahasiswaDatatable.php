<?php

namespace App\Livewire\Components\Admin;

use Livewire\Component;

class ProgressMahasiswaDatatable extends Component
{
    public function render()
    {
        return view('livewire.components.admin.progress-mahasiswa-datatable');
    }
}

<?php

namespace App\Livewire\Components\Admin\Dosen;

use App\Models\User;
use App\Models\Dosen;
use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Hash;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Facades\Response;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Protection;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;


class DosenManage extends Component
{
    use WithFileUploads,LivewireAlert;
    public $nama_dosen;
    public $singkatan_dosen;
    public $nip;
    public $nama_prodi = 'D3 Sistem Informasi'; // Isi sesuai nama prodi dari data yang diambil
    public $prodi = 'D3 SI';
    public $file;
    public $iteration_file = 1;

    public function render()
    {
        return view('livewire.components.admin.dosen.dosen-manage');
    }

    private function resetInputFields()
    {
        $this->nama_dosen = '';
        $this->singkatan_dosen = '';
        $this->nip = '';
    }

    public function exportTemplate()
    {
        $spreadsheet = new Spreadsheet();
        $worksheet   = $spreadsheet->getActiveSheet();

        $worksheet->mergeCells('A1:I2');

        $styleHeader = [
            'font' => [
                'bold'  => true,
                'color' => ['rgb' => '000000'],
                'size'  => 14,
                'name'  => 'Arial'
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical'   => Alignment::VERTICAL_CENTER,
            ],
            'fill' => [
                'fillType'   => Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => 'FFFF00',
                ],
            ],
        ];
        $styleHeader2 = [
            'font' => [
                'bold' => true,
                'color' => ['rgb' => '#000000'],
                'size' => 12,
                'name' => 'Arial'
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ],
        ];
        $styleIsiKolom = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ],
        ];

        $worksheet->getStyle('A1:I3')->applyFromArray($styleHeader);
        $worksheet->getStyle('A3:I3')->applyFromArray($styleHeader2);
        $worksheet->getStyle('A1:I503')->applyFromArray($styleIsiKolom);

        $worksheet->setCellValue('A1', 'Pendaftaran Akun Dosen');
        $worksheet->setCellValue('A3', 'Username');
        $worksheet->setCellValue('B3', 'Email');
        $worksheet->setCellValue('C3', 'Password');
        $worksheet->setCellValue('D3', 'Nama Dosen');
        $worksheet->setCellValue('E3', 'Nama Gelar');
        $worksheet->setCellValue('F3', 'NIP');
        $worksheet->setCellValue('G3', 'Kode Dosen');
        $worksheet->setCellValue('H3', 'alamat');
        $worksheet->setCellValue('I3', 'Telepon');


        $worksheet->getColumnDimension('A')->setWidth(35);
        $worksheet->getColumnDimension('B')->setWidth(35);
        $worksheet->getColumnDimension('C')->setWidth(35);
        $worksheet->getColumnDimension('D')->setWidth(35);
        $worksheet->getColumnDimension('E')->setWidth(35);
        $worksheet->getColumnDimension('F')->setWidth(35);
        $worksheet->getColumnDimension('G')->setWidth(35);
        $worksheet->getColumnDimension('H')->setWidth(35);
        $worksheet->getColumnDimension('I')->setWidth(35);
        $worksheet->getProtection()->setSheet(true);

        $worksheet->getStyle('A4:I503')->getProtection()->setLocked(Protection::PROTECTION_UNPROTECTED);

        $writer = new Xlsx($spreadsheet);

        $headers = [
            'Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Content-Disposition' => 'attachment; filename="'.'Pendaftaran Akun Dosen'.'.xlsx"',
        ];

        $response = Response::stream(
            function () use ($writer) {
                $writer->save('php://output');
            },
            200,
            $headers
        );

        return $response;
    }

    public function createDosen()
    {
        $this->validate([
            'file' => 'required|mimes:xlsx'
        ]);

        $reader      = IOFactory::createReaderForFile($this->file->getRealPath());
        $spreadsheet = $reader->load($this->file->getRealPath());
        $worksheet   = $spreadsheet->getActiveSheet();
        $rowCount    = $worksheet->getHighestDataRow();

        for ($row = 4; $row <= $rowCount + 1; $row++) {
            $username       = $worksheet->getCell('A' . $row)->getValue();
            $email          = $worksheet->getCell('B' . $row)->getValue();
            $password       = $worksheet->getCell('C' . $row)->getValue();
            $nama_dosen     = $worksheet->getCell('D' . $row)->getValue();
            $nama_gelar     = $worksheet->getCell('E' . $row)->getValue();
            $nip            = $worksheet->getCell('F' . $row)->getValue();
            $singkatan_dosen= $worksheet->getCell('G' . $row)->getValue();
            $alamat         = $worksheet->getCell('H' . $row)->getValue();
            $telepon        = $worksheet->getCell('I' . $row)->getValue();

            $cek = Dosen::where('email', $email)->count();

            if(empty($nama_dosen)){
                $this->clearFile();
                $this->flash('warning', 'Upload failed. Name is empty.');
            } else {
                if ($cek > 0) {
                    $this->clearFile();
                    $this->flash('warning', 'Duplicate data found.');
                } else {
                    $newDosen = Dosen::create([
                        'username' => $username,
                        'nama_gelar' => $nama_gelar,
                        'nama_dosen' => $nama_dosen,
                        'prodi' => $this->prodi,
                        'password' => Hash::make($password),
                        'role' => 'dosen',
                        'singkatan_dosen' => $singkatan_dosen,
                        'nip' => $nip,
                        'email' => $email,
                        'telepon' => $telepon,
                        'alamat' => $alamat,
                    ]);

                    if ($newDosen) {
                        $this->flash('success', 'Data berhasil disimpan.');
                        return redirect()->route('admin.dosen.index');
                    } else {
                        $this->flash('error', 'Gagal menyimpan data.');
                        return redirect()->route('admin.dosen.index');
                    }
                }
            }
        }
        $this->clearFile();
    }

    public function clearFile(){
        $this->file = null;
        $this->iteration_file++;
    }

}

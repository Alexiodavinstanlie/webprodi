<?php

namespace App\Livewire\Components\Admin\Dosen;

use App\Models\Dosen;
use Livewire\Component;
use Livewire\WithPagination;

class DosenDatatable extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $entries = 10;
    public $search;

    public $is_dospem_active = false;

    public function render()
    {
        $dosens = Dosen::query();
        if ($this->search) {
            $dosens = $dosens->where(function($query) {
                $query->where('nama_dosen', 'like', '%' . $this->search . '%')
                      ->orWhere('nip', 'like', '%' . $this->search . '%')
                      ->orWhere('singkatan_dosen', 'like', '%' . $this->search . '%');
            });
        }
        $dosens = $dosens->orderBy('created_at','DESC')->paginate($this->entries);

        $currentPage = $dosens->currentPage();
        $num = ($currentPage - 1) * $dosens->perPage() + 1;
        return view('livewire.components.admin.dosen.dosen-datatable',compact('dosens','currentPage','num',));
    }

    public function toggleDospem($dosenId)
    {
        $dosen = Dosen::find($dosenId);
        $dosen->is_dospem = !$dosen->is_dospem;
        $dosen->save();
        $this->is_dospem_active = $dosen->is_dospem;
    }

    public function toggleDospuj($dosenId)
    {
        $dosen = Dosen::find($dosenId);
        $dosen->is_dospuj = !$dosen->is_dospuj;
        $dosen->save();
    }

    public function toggleDoswal($dosenId)
    {
        $dosen = Dosen::find($dosenId);
        $dosen->is_doswal = !$dosen->is_doswal;
        $dosen->save();
    }
}

<?php

namespace App\Livewire\Components\Admin\Roledosen;

use Livewire\Component;

class RoledosenDatatable extends Component
{
    public function render()
    {
        return view('livewire.components.admin.roledosen.roledosen-datatable');
    }
}

<?php

namespace App\Livewire\Components\Admin\Dashboard;

use Livewire\Component;
use App\Models\TahunAjaran;
use Illuminate\Http\Request;
use Livewire\WithPagination;
use App\Models\CurrentSemester;
use Illuminate\Support\Facades\Auth;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class AdminDashboardDatatable extends Component
{
    use WithPagination, LivewireAlert;
    public    $entries         = 10;
    public    $options         = [5, 10, 15, 20];
    protected $paginationTheme = 'bootstrap';
    public    $mode            = 'create';
    public $search;

    public $is_active = 0;
    public $maxTahunAjaranCount = 1;
    public $tahunAjaranCount = 0;

    public $tahun_ajaran_awal;
    public $tahun_ajaran_akhir;
    public $tahun_ajaran_awal_aktif;
    public $tahun_ajaran_akhir_aktif;
    public $semester;


    public function mount()
    {
        // Mengambil nilai semester dari database CurrentSemester
        $currentSemester = CurrentSemester::first(); // Misalnya, ambil semester pertama (sesuaikan dengan logika Anda)
        if ($currentSemester) {
            $this->semester = $currentSemester->semester;
        }
    }

    public function render()
    {
        $tahunAjarans = TahunAjaran::query();
        if ($this->search) {
            $tahunAjarans = $tahunAjarans->where(function($query) {
                $query->where('tahun_ajaran_awal', 'like', '%' . $this->search . '%')
                      ->orWhere('tahun_ajaran_akhir', 'like', '%' . $this->search . '%');
            });
        }
        $tahunAjarans = $tahunAjarans->orderByDesc('is_active')
        ->orderByDesc('created_at')
        ->paginate($this->entries);
        $currentPage = $tahunAjarans->currentPage();
        $num = ($currentPage - 1) * $tahunAjarans->perPage() + 1;
        $this->getActiveTahunAjaran();
        return view('livewire.components.admin.dashboard.admin-dashboard-datatable',compact('tahunAjarans','currentPage','num'));
    }

    public function createData()
    {
        $this->validate([
            'tahun_ajaran_awal' => 'required',
            'tahun_ajaran_akhir' => 'required',
        ]);

        TahunAjaran::create([
            'tahun_ajaran_awal' => $this->tahun_ajaran_awal,
            'tahun_ajaran_akhir' => $this->tahun_ajaran_akhir,
            // 'tahun_ajaran' => $this->tahun_ajaran_awal . ' - ' . $this->tahun_ajaran_akhir,
            'is_active' => 0, // Misalnya, atur aktif sebagai 1 (aktif) secara default
        ]);

        $this->resetInputFields();
        $this->dispatch('closeModalTahunAjaran');
        $this->alert('success', 'Data Periode berhasil disimpan');
    }

    public function showModal($mode, $id_mutu = null)
    {
        $this->mode = $mode;

        if ($id_mutu) {
            // $this->mutuId = TahunAjaran::findOrFail($id_mutu);
            // $this->kode_mutu = $this->mutuId->kode_mutu;
            // $this->nilai_min = $this->mutuId->nilai_min;
            // $this->nilai_max = $this->mutuId->nilai_max;

        }

        $this->dispatch('openModalTahunAjaran');
    }

    private function resetInputFields()
    {
        $this->tahun_ajaran_awal = '';
        $this->tahun_ajaran_akhir = '';
        // ...reset field lainnya jika ada
    }

    public function isActive($tahunAjaransId)
    {
        $tahunajaran = TahunAjaran::findOrFail($tahunAjaransId);
        $this->tahunAjaranCount = TahunAjaran::where('is_active', true)->count();

        if ($tahunajaran->is_active) {
            $tahunajaran->is_active = false;
            $tahunajaran->save();
        } else {
            if ($this->tahunAjaranCount < $this->maxTahunAjaranCount) {
                $tahunajaran->is_active = true;
                $tahunajaran->save();
            } else {
                $this->alert('warning', 'Tahun Ajaran sudah mencapai batas maksimal.');
            }
        }
    }

    public function getActiveTahunAjaran()
    {
        $tahunAjaranAktif = TahunAjaran::where('is_active', true)->first();

        if ($tahunAjaranAktif) {
            $this->tahun_ajaran_awal_aktif = $tahunAjaranAktif->tahun_ajaran_awal; // Sesuaikan dengan nama kolom yang benar
            $this->tahun_ajaran_akhir_aktif = $tahunAjaranAktif->tahun_ajaran_akhir; // Sesuaikan dengan nama kolom yang benar
        } else {
            // Tindakan jika tidak ada tahun ajaran yang aktif
        }
    }

    public function submitsemester(Request $request)
    {
        if (empty($this->semester)) {
            $this->alert('warning', 'Harap pilih semester terlebih dahulu.');
        } else {
            $this->validate([
                'semester' => 'required',
            ]);


            $semester = $request->input('semester');
            $tahunAjaranAktif = TahunAjaran::where('is_active', true)->first();

        $semester_data = CurrentSemester::first();
        if ($tahunAjaranAktif) {
            $tahun_ajaran = $tahunAjaranAktif->tahun_ajaran_awal . ' - ' . $tahunAjaranAktif->tahun_ajaran_akhir;
            $currentSemester = CurrentSemester::first();

            if ($semester_data) {
                $semester_data->update([
                    'tahun_ajaran' => $this->tahun_ajaran_awal_aktif . ' - ' . $this->tahun_ajaran_akhir_aktif,
                    'semester' => $this->semester,
                ]);
                $this->alert('success', 'Data berhasil diperbarui.');
            } else {
                // Jika tidak ada, buat entri baru
                CurrentSemester::create([
                    'tahun_ajaran' => $this->tahun_ajaran_awal_aktif . ' - ' . $this->tahun_ajaran_akhir_aktif,
                    'semester' => $this->semester,
                ]);
                $this->alert('success', 'Data berhasil disimpan.');
            }

        }

    }}
}


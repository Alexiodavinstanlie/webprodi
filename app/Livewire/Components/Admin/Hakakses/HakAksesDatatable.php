<?php

namespace App\Livewire\Components\Admin\Hakakses;

use Livewire\Component;

class HakAksesDatatable extends Component
{
    public function render()
    {
        return view('livewire..components.admin.hakakses.hak-akses-datatable');
    }
}

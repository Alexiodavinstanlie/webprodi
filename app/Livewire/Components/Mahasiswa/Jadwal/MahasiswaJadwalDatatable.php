<?php

namespace App\Livewire\Components\Mahasiswa\Jadwal;

use Livewire\Component;

class MahasiswaJadwalDatatable extends Component
{
    public function render()
    {
        return view('livewire.components.mahasiswa.jadwal.mahasiswa-jadwal-datatable');
    }
}

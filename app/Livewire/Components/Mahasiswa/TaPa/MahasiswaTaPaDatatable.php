<?php

namespace App\Livewire\Components\Mahasiswa\TaPa;

use Livewire\Component;

class MahasiswaTaPaDatatable extends Component
{
    public function render()
    {
        return view('livewire.components.mahasiswa.ta-pa.mahasiswa-ta-pa-datatable');
    }
}

<?php

namespace App\Livewire\Components\Mahasiswa\Nilai;

use Livewire\Component;

class MahasiswaNilaiDatatable extends Component
{
    public function render()
    {
        return view('livewire.components.mahasiswa.nilai.mahasiswa-nilai-datatable');
    }
}

<?php

namespace App\Livewire\Components\Dosen\DosenPenguji;

use Livewire\Component;

class DosenDospujDatatable extends Component
{
    public function render()
    {
        return view('livewire.components.dosen.dosen-penguji.dosen-dospuj-datatable');
    }
}

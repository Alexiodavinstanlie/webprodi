<?php

namespace App\Livewire\Components\Dosen\DosenWali;

use Livewire\Component;

class DosenDoswalDatatable extends Component
{
    public function render()
    {
        return view('livewire.components.dosen.dosen-wali.dosen-doswal-datatable');
    }
}

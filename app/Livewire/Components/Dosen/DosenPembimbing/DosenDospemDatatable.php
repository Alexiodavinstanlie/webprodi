<?php

namespace App\Livewire\Components\Dosen\DosenPembimbing;

use Livewire\Component;

class DosenDospemDatatable extends Component
{
    public function render()
    {
        return view('livewire.components.dosen.dosen-pembimbing.dosen-dospem-datatable');
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectTo() {
        $role = Auth::user()->role;
        switch ($role) {
            case 2:
                return route('admin.dashboard');
                break;
            case 3:
                return route('kaprodi.dashboard');
                break;
            case 0:
                return route('mahasiswa.dashboard');
                break;
            case 1:
                return route('dosen.dashboard');
                break;
            case 4:
                return route('superadmin.dashboard');
                break;
            default:
                // return Session::get('previous', url('/'));
                break;
        }
    }
}

<?php

namespace Database\Factories;
use Faker\Generator as Faker;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Ruangan>
 */
class RuanganFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'kode_ruangan' => $this->faker->unique()->numerify('RU###'),
            'nama_ruangan' => $this->faker->word,
            'keterangan_ruangan' => $this->faker->sentence,
            'status' => $this->faker->randomElement(['Tersedia', 'Tidak Tersedia']),
        ];
    }
}

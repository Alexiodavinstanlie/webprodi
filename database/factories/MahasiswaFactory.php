<?php

namespace Database\Factories;

use App\Models\periode;
use App\Models\Mahasiswa;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Mahasiswa>
 */
class MahasiswaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Mahasiswa::class;


    public function definition()
    {
        $this->faker->addProvider(new \Faker\Provider\id_ID\Person($this->faker));
        return [
            'nama_mahasiswa' => $this->faker->name,
            'nim' => $this->faker->unique()->randomNumber(8, true),
            'nama_prodi' => 'D3 Sistem Informasi',
            'singkatan_prodi' => 'D3 SI',
            'tahun_masuk' => $this->faker->year,
            'periode_id' => 1,
            'email' => $this->faker->unique()->safeEmail,
        ];
    }
}

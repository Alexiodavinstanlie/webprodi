<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\NilaiMutu>
 */
class NilaiMutuFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'kode_mutu' => $faker->randomLetter, // Ganti ini sesuai dengan kebutuhan Anda
            'nilai_min' => $faker->numberBetween(0, 100), // Ganti ini sesuai dengan kebutuhan Anda
            'nilai_max' => $faker->numberBetween(0, 100), // Ganti ini sesuai dengan kebutuhan Anda
            'current_semester_id' => 1, // Ganti ini sesuai dengan id periode yang sesuai
        ];
    }
}

<?php

namespace Database\Factories;

use App\Models\Dosen;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Dosen>
 */
class DosenFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $this->faker->addProvider(new \Faker\Provider\id_ID\Person($this->faker));

        $nama_dosen = $this->faker->name;
        $nama_dosen = ucwords($nama_dosen); // Mengonversi semua kata dalam nama menjadi huruf besar
        $inisial = '';

        $parts = explode(' ', $nama_dosen);
        foreach ($parts as $part) {
            $inisial .= strtoupper($part[0]); // Mengambil 3 huruf pertama dan mengonversinya ke huruf besar
        }
        return [
            'nama_dosen' => $nama_dosen,
            'prodi' => 'D3SI',
            'role' => 'dosen',
            'nip' => $this->faker->unique()->randomNumber(8, true),
            'singkatan_dosen' => $inisial,
            'email' => $this->faker->unique()->safeEmail,
            'current_semester_id' => 1,
            'username' => $this->faker->unique()->userName, // Generate username
            'password' => bcrypt($this->faker->password), // Generate password (note: gunakan fungsi yang sesuai dengan platform atau framework Anda)
            'nama_gelar' => $this->faker->suffix . ' ' . $this->faker->firstNameMale, // Generate nama gelar
            'telepon' => $this->faker->numerify('+#######'), // Generate nomor telepon
            'alamat' => $this->faker->address, // Generate alamat
        ];
    }
}

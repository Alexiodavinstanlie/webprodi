<?php

namespace Database\Factories;
use App\Models\periode;
use Faker\Generator as Faker;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Periode>
 */
class PeriodeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

        $semester = $this->faker->randomElement(['Genap', 'Ganjil']);
        $tahun = $this->faker->year;
        $bulan = $this->faker->randomElement($months);
        $periode = $tahun . ' - ' . ($tahun + 1);

        return [
            'periode' => $periode,
            'tahun' => $tahun,
            'semester' => $semester,
            'bulan' => $bulan,
        ];
    }
}

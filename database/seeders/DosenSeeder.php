<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Dosen;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DosenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */

    public function run(): void
    {
        Dosen::factory(100)->create();

        $dosens = Dosen::all();
        foreach ($dosens as $key => $dosen) {
            $name = $dosen->nama_dosen;
            $slug = Str::slug($name, '-');
            $count = 2; // atau gunakan nilai yang lebih besar jika diperlukan

            while (User::where('slug', $slug)->exists()) {
                $slug = Str::slug($name, '-') . '-' . $count;
                $count++;
            }

            $user =  User::create([
                'name'              => $name,
                'email'             => $dosen->email,
                'email_verified_at' => now(),
                'password'          => Hash::make('dosen123'),
                'role'              => User::ROLE_DOSEN,
                'slug'              =>$slug
            ]);

            $dosen->update([
                'user_id' => $user->id
            ]);
        }
    }
}

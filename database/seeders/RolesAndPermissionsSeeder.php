<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $superadminRole = Role::create(['name' => 'superadmin']);
        $adminRole = Role::create(['name' => 'admin']);
        $mahasiswaRole = Role::create(['name' => 'mahasiswa']);
        $dosenRole = Role::create(['name' => 'dosen']);
        $kaprodiRole = Role::create(['name' => 'kaprodi']);

        $manageUsersPermission = Permission::create(['name' => 'manage_users']);
        $managePostsPermission = Permission::create(['name' => 'manage_posts']);

        $superadminRole->givePermissionTo($manageUsersPermission);
        $adminRole->givePermissionTo($manageUsersPermission);
        $adminRole->givePermissionTo($managePostsPermission);
    }

}

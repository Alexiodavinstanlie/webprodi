<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Mahasiswa;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class MahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Mahasiswa::factory(100)->create();

        $mahasiswas = Mahasiswa::all();
        foreach ($mahasiswas as $key => $mahasiswa) {
            $name = $mahasiswa->nama_mahasiswa;
            $slug = Str::slug($name,'-');
            if(User::where('slug',$slug)->first() !=null)
                $slug = $slug.'-' .time();

            $user =  User::create([
                'name'              => $name,
                'email'             => $mahasiswa->email,
                'email_verified_at' => now(),
                'password'          => Hash::make('mahasiswa123'),
                'role'              => User::ROLE_MAHASISWA,
                'slug'              => $slug
            ]);

            $mahasiswa->update([
                'user_id' => $user->id
            ]);
        }
    }
}

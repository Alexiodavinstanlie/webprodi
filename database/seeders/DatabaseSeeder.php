<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\periode;
use Illuminate\Database\Seeder;
use Database\Seeders\UserSeeder;
use Database\Seeders\DosenSeeder;
use Database\Seeders\PeriodeSeeder;
use Database\Seeders\RuanganSeeder;
use Database\Seeders\MahasiswaSeeder;
use Database\Seeders\NilaiMutuSeeder;
use Database\Seeders\TahunAjaranSeeder;
use Database\Seeders\CurrentSemesterSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(UserSeeder::class);
        $this->call(TahunAjaranSeeder::class);
        $this->call(CurrentSemesterSeeder::class);
        $this->call(PeriodeSeeder::class);
        $this->call(MahasiswaSeeder::class);
        $this->call(NilaiMutuSeeder::class);
        $this->call(DosenSeeder::class);
        $this->call(RuanganSeeder::class);


    }
}

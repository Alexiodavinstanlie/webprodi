<?php

namespace Database\Seeders;

use App\Models\Nilai_mutu;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class NilaiMutuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Nilai_mutu::create([
            'kode_mutu' => 'A',
            'nilai_min' => 81,
            'nilai_max' => 100,
            'current_semester_id' => 1,
        ]);
        Nilai_mutu::create([
            'kode_mutu' => 'AB',
            'nilai_min' => 71,
            'nilai_max' => 80,
            'current_semester_id' => 1,
        ]);

        Nilai_mutu::create([
            'kode_mutu' => 'B',
            'nilai_min' => 66,
            'nilai_max' => 70,
            'current_semester_id' => 1,
        ]);
        Nilai_mutu::create([
            'kode_mutu' => 'BC',
            'nilai_min' => 61,
            'nilai_max' => 65,
            'current_semester_id' => 1,
        ]);
        Nilai_mutu::create([
            'kode_mutu' => 'C',
            'nilai_min' => 51,
            'nilai_max' => 60,
            'current_semester_id' => 1,
        ]);
        Nilai_mutu::create([
            'kode_mutu' => 'D',
            'nilai_min' => 41,
            'nilai_max' => 50,
            'current_semester_id' => 1,
        ]);
        Nilai_mutu::create([
            'kode_mutu' => 'E',
            'nilai_min' => 0,
            'nilai_max' => 40,
            'current_semester_id' => 1,
        ]);
    }
}

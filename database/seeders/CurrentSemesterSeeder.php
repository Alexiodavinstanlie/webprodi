<?php

namespace Database\Seeders;

use App\Models\TahunAjaran;
use App\Models\CurrentSemester;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CurrentSemesterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Mengambil data terakhir dari TahunAjaran
        $latestTahunAjaran = TahunAjaran::orderBy('id', 'desc')->first();

        // Mengekstrak tahun ajaran terakhir
        $tahunAjaran = $latestTahunAjaran->tahun_ajaran_awal . '/' . $latestTahunAjaran->tahun_ajaran_akhir;

        // Mengisi tabel current_semesters dengan data terakhir dari TahunAjaran
        CurrentSemester::create([
            'tahun_ajaran' => $tahunAjaran,
            'semester' => 'genap',
        ]);
    }
}

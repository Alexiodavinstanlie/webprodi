<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $dosen = User::create([
            'name'              => 'dosen',
            'email'             => 'dosen@gmail.com',
            'email_verified_at' => now(),
            'password'          => Hash::make('dosen123'),
            'role'              => User::ROLE_DOSEN,
            'slug'              => 'dosen'
        ]);

        $superAdmin = User::create([
            'name'              => 'superAdmin',
            'email'             => 'superadmin@gmail.com',
            'email_verified_at' => now(),
            'password'          => Hash::make('superadmin123'),
            'role'              => User::ROLE_SUPERADMIN,
            'slug'              => 'superadmin'
        ]);

        $admin = User::create([
            'name'              => 'admin',
            'email'             => 'admin@gmail.com',
            'email_verified_at' => now(),
            'password'          => Hash::make('admin123'),
            'role'              => User::ROLE_ADMIN,
            'slug'              => 'admin'
        ]);

        $mahasiswa = User::create([
            'name'              => 'mahasiswa',
            'email'             => 'mahasiswa@gmail.com',
            'email_verified_at' => now(),
            'password'          => Hash::make('mahasiswa123'),
            'role'              => User::ROLE_MAHASISWA,
            'slug'              => 'mahasiswa'
        ]);

        $kaprodi = User::create([
            'name'              => 'kaprodi',
            'email'             => 'kaprodi@gmail.com',
            'email_verified_at' => now(),
            'password'          => Hash::make('kaprodi123'),
            'role'              => User::ROLE_KAPRODI,
            'slug'              => 'kaprodi'
        ]);
    }
}


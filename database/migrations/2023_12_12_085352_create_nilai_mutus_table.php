<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('nilai_mutus', function (Blueprint $table) {
            $table->id();
            $table->string('kode_mutu');
            $table->integer('nilai_min');
            $table->integer('nilai_max');
            $table->unsignedBigInteger('current_semester_id')->nullable();
            $table->foreign('current_semester_id')->references('id')->on('current_semesters');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('nilai_mutus');
    }
};

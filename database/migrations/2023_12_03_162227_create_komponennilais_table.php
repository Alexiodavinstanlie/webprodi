<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('komponennilais', function (Blueprint $table) {
            $table->id();
            $table->string('nama')->nullable();
            $table->integer('nilai_komponen')->nullable();
            $table->string('nama_prasidang')->nullable();
            $table->integer('nilai_komponen_prasidang')->nullable();
            $table->string('nama_sidang')->nullable();
            $table->integer('nilai_komponen_sidang')->nullable();
            $table->boolean('is_active')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('komponennilais');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('mahasiswa_sidangs', function (Blueprint $table) {
            $table->id();
            $table->BigInteger('pendaftaran_sidang_id')->nullable()->unsigned();
            $table->BigInteger('mahasiswa_id')->nullable()->unsigned();
            $table->BigInteger('pembimbing1_id')->nullable()->unsigned();
            $table->BigInteger('pembimbing2_id')->nullable()->unsigned();
            $table->BigInteger('penguji1_id')->nullable()->unsigned();
            $table->BigInteger('penguji2_id')->nullable()->unsigned();
            $table->BigInteger('periode_id')->nullable()->unsigned();
            $table->string('judul_indo')->nullable();
            $table->string('judul_inggris')->nullable();
            $table->string('tahun_ajaran')->nullable();
            $table->string('semester')->nullable();
            $table->string('bulan')->nullable();
            $table->integer('jumlah_penguji')->default(2);
            $table->timestamps();

            $table->foreign('pendaftaran_sidang_id')->references('id')->on('pendaftaran_sidangs')->onDelete('cascade');
            $table->foreign('mahasiswa_id')->references('id')->on('mahasiswas')->onDelete('cascade');
            $table->foreign('pembimbing1_id')->references('id')->on('dosens')->onDelete('cascade');
            $table->foreign('pembimbing2_id')->references('id')->on('dosens')->onDelete('cascade');
            $table->foreign('penguji1_id')->references('id')->on('dosens')->onDelete('cascade');
            $table->foreign('penguji2_id')->references('id')->on('dosens')->onDelete('cascade');
            $table->foreign('periode_id')->references('id')->on('periodes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mahasiswa_sidangs');
    }
};

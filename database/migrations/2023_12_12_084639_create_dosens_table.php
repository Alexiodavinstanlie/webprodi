<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('dosens', function (Blueprint $table) {
            $table->id();
            $table->string('username');
            $table->string('nama_dosen');
            $table->string('nama_gelar');
            $table->string('prodi');
            $table->string('role');
            $table->string('singkatan_dosen');
            $table->string('nip');
            $table->boolean('is_admin')->default(false); // Kolom untuk menandai apakah dosen adalah admin
            $table->boolean('is_dospem')->default(false); // Kolom untuk menandai apakah dosen adalah dosen pembimbing
            $table->boolean('is_dospuj')->default(false); // Kolom untuk menandai apakah dosen adalah dosen penguji
            $table->boolean('is_doswal')->default(false);
            $table->boolean('is_kaprodi')->default(false);
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('telepon');
            $table->string('alamat');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('current_semester_id')->nullable();
            $table->foreign('current_semester_id')->references('id')->on('current_semesters');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('dosens');
    }
};
